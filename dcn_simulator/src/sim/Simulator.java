package sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.opencsv.CSVReader;
import com.thoughtworks.xstream.XStream;

import network.Network;
import network.NetworkBuilder;
import network.Session;

/**
 * Implementation of simulator
 * <p> Provides methods to create Networks, generate flows, save and load Session from XML files, 
 * run the routing algorithm, etc.
 * @author Sebastian Fehlandt
 *
 */
public class Simulator {

	//******************************** ATTRIBUTES **********************************
	// Folders in user directory:
	public static final String USER_DIR    = "user_files";
	public static final String NET_DIR     = USER_DIR + File.separator + "net";
	public static final String SESSION_DIR = USER_DIR + File.separator + "sessions";
	public static final String OUT_DIR     = USER_DIR + File.separator + "output";
	
	// Filename prefixes and suffixes:
	public static final String BUILD_EXT    = ".csv";
	public static final String LOAD_EXT     = ".xml";
	public static final String OUT_DEF_FILE = "session_";
	
	private Messenger msn;
	private int algVerbosity = RoutingAlgorithm.VERBOSITY_FLOW_PROGRESS;
	
	// Attributes for file dealing:
	protected File inputFile;
	protected File sessionFile;
	
	// Attributes for simulation and modelling:
	protected Session ses;            // Session object
	protected RoutingAlgorithm alg;   // Routing algorithm object
	
	//****************************** CONSTRUCTOR ***********************************
	/**
     * Constructs a new Simulator using {@link System#out} to construct a 
     * {@link PrintStreamMessenger} as {@link Messenger}
     */
	public Simulator() {
		this.msn = new PrintStreamMessenger(System.out);
	}
	
	/**
     * Constructs a new Simulator.
     * @param msn {@link Messenger} to send output messages
     * @throws IOException
     */
	public Simulator(Messenger msn) throws IOException{
		this.msn = msn; // Set output stream
	}
	
	//******************** METHODS FOR EXECUTION OF SIMULATIONS ********************
	/**
     * Runs a single simulation according to a standard configuration file
     * @param duration, duration of the simulation 
     * (currently not used, but provided for further improvements in the project)
     * @param configFile name of the configuration file.
     * @throws IOException
     */
	public void runSingleSim(float duration, String configFile) throws IOException{
		readSingleSimFile(configFile);              // read configuration file
		setRoutingAlgorithm(new MRGRouting());   // set the routing algorithm
		runSimulation(MRGRouting.MR_GREEN); // run simulation
		saveSession();                           // save session file
	}
	
	/**
     * Reads a configuration file for a single simulation but does not perform the simulation
     * @param configFileName name of the configuration file.
     * @throws IOException
     */
	public void readSingleSimFile(String configFileName) throws IOException{
		// Open file:
		File file = new File(USER_DIR, configFileName);
		msn.sendMessage("Running simulation according to configuration file: " + file);
		
		CSVReader reader = new CSVReader(new FileReader(file));
		List<String[]> lines = reader.readAll();
		reader.close();
		
		//---- Read network parameters
		boolean loadFlag =   lines.get(0)[1].trim().equals("1"); // flag: 0 - build network, 1 - load session
		String inFileName =  lines.get(1)[1];                    // input network/session file
		
		//---- Read traffic parameters		
		boolean resetFlag  = lines.get(3)[1].trim().equals("0"); //flag: 0-reset traffic, 1-not
		boolean staticFlag = lines.get(4)[1].trim().equals("1"); //flag: 1-static, 0-not
		int   numFlows =          Integer.parseInt(lines.get(5)[1].trim()); // number of flows		
		float duration = (float)Double.parseDouble(lines.get(6)[1].trim()); // duration of simulation
		float mean     = (float)Double.parseDouble(lines.get(7)[1].trim()); // Normal dist mean for demand generation
		float stdDev   = (float)Double.parseDouble(lines.get(8)[1].trim()); // Normal dist std dev for demand gen.
		
		//---- Destination session file
		String outFileName = lines.get(10)[1]; // output session file
		
		//---- Load or Build Network
		if (loadFlag){
			loadSession(inFileName);
			ses.resetState(false);
		} else {
			buildNetwork(inFileName);
		}
		
		//---- Generate traffic
		if (resetFlag){
			if (staticFlag) {
				ses.genNormalStaticTraffic(numFlows, mean, stdDev);
			} else {
				ses.genNormalTraffic(numFlows, mean, stdDev, duration);
			}
		}		
		setSessionFile(outFileName); // set output session file
	}
	
	/**
	 * Generates a random Static Traffic based on a normal distribution
     * @param numFlows the number of flows
     * @param mean mean of the normal distribution
     * @param stdDev standard deviation of the normal distribution
	 */
	public void generateStaticTraffic(int numFlows, float mean, float stdDev){
		ses.genNormalStaticTraffic(numFlows, mean, stdDev);
	}
	
	/**
	 * Generates a random Static Traffic based on a normal distribution
     * @param numFlows the number of flows
     * @param mean mean of the normal distribution
     * @param stdDev standard deviation of the normal distribution
     * @param duration time after which the simulation ends 
     */
	public void generateTraffic(int numFlows, float mean, float stdDev, float duration){
		resetSimulation();
		ses.genNormalTraffic(numFlows, mean, stdDev,duration);
	}
	
	public final Session getSession(){
		return ses;
	}
	
	/** Resets the state of the Session and simulation as if was never simulated */
	public void resetSimulation(){
		alg.init(ses, msn);
		alg.reset();
	}
	
	/** 
	 * Runs a simulation based on parameters previously read 
	 * @param subalgorithm integer describing the sub-algorithm to execute, 0 is the default sub-algorithm
	 * @param verbosity level to control the level of detail of the displayed messages
	 * @see RoutingAlgorithm
	 * */
	public void runSimulation(int subalgorithm, int verbosity){
		this.algVerbosity = verbosity;
		runSimulation(subalgorithm);
	}
	
	/** 
	 * Runs a simulation based on parameters previously read 
	 * @param subalgorithm integer describing the sub-algorithm to execute, 0 is the default sub-algorithm
	 * */
	public void runSimulation(int subalgorithm){
		resetSimulation();
		alg.run(subalgorithm, algVerbosity);
	}
	
	/**
	 * Sets the desired {@link RoutingAlgorithm} to be used by the simulator
	 * @param alg {@link RoutingAlgorithm} to be set
	 */
	public void setRoutingAlgorithm(RoutingAlgorithm alg){
		this.alg = alg;
		alg.init(ses, msn);
	}
	
	//**************** METHODS TO LOAD/SAVE FILES & BUILD NETWORKS *****************
	/**
     * Builds a network with parameters given by a file
     * @param networkFileName name of the network file.
     * @throws IOException
     * @throws FileNotFoundException
     */
	public void buildNetwork(String networkFileName) throws FileNotFoundException, IOException{
		
		//---- Read File to List of String arrays:
		inputFile = new File(NET_DIR, prepareFileName(networkFileName, BUILD_EXT));
		CSVReader reader = new CSVReader(new FileReader(inputFile));
		List<String[]> lines = reader.readAll();
		reader.close();
				
		//---- Extract data from List of String arrays:
		String arch                 = lines.get(0)[1];      // architecture to use
		String [] networkArgs       = lines.get(2);         // network parameters array
		String serverParamsFilename = lines.get(4)[1];      // server params file
		String switchParamsFilename = lines.get(8)[1];      // switch params file
		String [] serverArgs        = lines.get(6);         // server parameters array
		String [] switchArgs        = lines.get(10);        // switch parameters array
		File serverParamsFile       = null;                 // server parameters filename
		File switchParamsFile       = null;                 // switch parameters filename
		
		//---- If there is a server parameters filename in the next cell we use that file and overwrite serverArgs
		if (serverParamsFilename != null && !serverParamsFilename.equals("")){
			
			serverParamsFile = new File(NET_DIR, Simulator.prepareFileName(serverParamsFilename, BUILD_EXT));			
			String [] aux = readParametersFile(serverParamsFile);
			
			if (aux != null)
				serverArgs = aux;
			else
				msn.sendWarning("Server file gave no parameters (" + serverParamsFile + 
						"), parameters of networkfile used instead ");
		}
		
		//---- If there is a switch parameters filename in the next cell we use that file and overwrite switchArgs
		if (switchParamsFilename != null && !switchParamsFilename.equals("")){
			
			switchParamsFile = new File(NET_DIR, Simulator.prepareFileName(switchParamsFilename, BUILD_EXT));			
			String [] aux = readParametersFile(switchParamsFile);
			
			if (aux != null)
				switchArgs = aux;
			else
				msn.sendWarning("Switch file gave no parameters (" + switchParamsFile + 
						"), parameters of networkfile used instead ");
		}
		
		//---- Print network parameters:
		msn.sendMessage("Network parameters = " + Arrays.toString(networkArgs));		
		if (serverParamsFile != null) msn.sendMessage("Server file = " + serverParamsFile);		
		if (switchParamsFile != null) msn.sendMessage("Switch file = " + switchParamsFile);		
		msn.sendMessage("Server parameters = " + Arrays.toString(serverArgs));
		msn.sendMessage("Switch parameters = " + Arrays.toString(switchArgs));
		
		//---- Build network and session
		Network net = NetworkBuilder.buildNetwork(networkArgs, serverArgs, switchArgs, arch, msn);
		this.ses = new Session(net, false);
		msn.sendMessage("Network built successfully with " + 
				net.getNumNodes()    + " nodes, " + 
				net.getNumServers()  + " " + net.getServerLabel(true) + " and " +
				net.getNumSwitches() + " " + net.getSwitchLabel(true));
	}
	
	/**
     * Loads a session file
     * @param sessionFileName name of the session file.
     * @throws FileNotFoundException
     */
	public void loadSession(String sessionFileName) throws FileNotFoundException{
		inputFile = new File(SESSION_DIR, prepareFileName(sessionFileName, LOAD_EXT));
		XStream xstream = new XStream();
		FileInputStream fis = new FileInputStream(inputFile);
		ses = (Session) xstream.fromXML(fis);
		msn.sendMessage("Session loaded succesfully from file: " + inputFile);
	}
	
	/**
     * Saves the session to a previously set sessionFile
     * @throws FileNotFoundException
     */
	public void saveSession() throws FileNotFoundException{
		if (sessionFile == null) setSessionFile(null);
		
		XStream xstream = new XStream();
		FileOutputStream fs = new FileOutputStream(sessionFile);
		xstream.toXML(ses, fs);
		
		msn.sendMessage("Session file written successfully: " + sessionFile);
	}
	
	/**
     * Saves the session to a given file
     * @param sessionFileName name of the session file.
     * @throws FileNotFoundException
     */
	public void saveSession(String sessionFileName) throws FileNotFoundException{
		setSessionFile(sessionFileName);
		saveSession();
	}
	
	/**
     * Sets the output file to save the session
     * @param sessionFileName name of the session file.
     */
	public void setSessionFile(String sessionFileName){
		if (sessionFileName == null || sessionFileName.equals("")){
			String timestamp =  new java.text.SimpleDateFormat("MM/dd/yyyy h:mm:ss a").format(new Date());
			timestamp = timestamp.replaceAll(" ", "_");
			timestamp = timestamp.replaceAll("/", "-");
			sessionFileName = OUT_DEF_FILE + timestamp;			
		}
		sessionFile = new File(SESSION_DIR, prepareFileName(sessionFileName, LOAD_EXT));
		
		msn.sendMessage("Output session file set to: " + sessionFile);		
		if (sessionFile.equals(inputFile)) {
			msn.sendWarning("Input and output session files are equal, input file will be overwritten");
		}
	}
	
	//************************* PRIVATE AUXILIARY METHODS **************************
	/**
     * Checks if a filename contains a given extension and returns the name with extension
     * @param fileName name of the file
     * @param fileExt  extension of the file
     * @return the name with the added extension
     */
	private static String prepareFileName(String fileName, String fileExt){
		String file = fileName.trim();                                 // eliminate blanks before and after fileName
		String ext = file.substring( file.length()-4 , file.length()); // get extension 
		if (!ext.equalsIgnoreCase(fileExt))	file = file +  fileExt;	   // if its not the same extension we add it
		return file;
	}
	
	/** Returns an array of parameters in String format, read from a given .csv file
	 *  @param file File to read
	 *  @return array of parameters as String
	 */
	private static String[] readParametersFile(File file) throws IOException{
		CSVReader reader = new CSVReader(new FileReader(file));
		List<String[]> lines = reader.readAll();	
		reader.close();
		String[] line = lines.get(1);
		return Arrays.copyOf(line, line.length);
	}
	
	//*************************** MAIN FUNCTION ************************************
	/** Main function, use to execute simulation according to configuration file "sim_config.csv"
	 *  @param args unused (for now)
	 *  @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		
		Simulator simulator = new Simulator(new PrintStreamMessenger());
		simulator.readSingleSimFile("single_sim.csv");
		simulator.setRoutingAlgorithm(new MRGRouting());		
		simulator.runSimulation(MRGRouting.MR_GREEN);
		simulator.saveSession();
		
	}
}
