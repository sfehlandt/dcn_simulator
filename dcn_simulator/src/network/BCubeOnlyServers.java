package network;

/**
 * Implementation of a BCube network with only servers, implementing the {@link Network} interface
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class BCubeOnlyServers extends BCube {

	//****************************** CONSTRUCTORS **********************************
	/**
     * Construct a new BCube network with only servers.
     * @param netArgs    array of parameters to construct the network,  parsed as Strings.
     * @param serverArgs array of parameters to construct the servers,  parsed as Strings.
     */
	public BCubeOnlyServers(String[] netArgs, String[] serverArgs) {
		super(netArgs, serverArgs, null);
	}
	
	//******************************** METHODS *************************************
	@Override
	protected void createDevices(){
		int n = numPorts; // BCube n parameter
   		int k = maxLevel; // BCube_k
		
		//---- Create switches ----//
   		for (int level = 0; level <= k; level++)			// for each level
   			for (int sw = 0; sw < switchesPerLevel; sw++)	// for each switch
   				switches.set(level,sw,new Server(switchesCounter++, nodesCounter++, n, level, serverParams, 
   						new Address(prefix,level,sw,n), true));
   		
   		//---- Create servers ----//
   		int i = 0;
   		for (int sw = 0; sw < switchesPerLevel; sw++){	// for each switch
   			for (int sv = 0; sv < n; sv++){				// for each server
   				servers[i] = new Server(serversCounter++, nodesCounter++, k+1, 0, serverParams, 
   						new Address(prefix,0,sw,sv));
   				i++;
   			}   			
   		}
	}
	
	@Override
	public String getServerLabel(boolean plural) {
		if (plural)
			return "end-hosts";
		else
			return "end-host";
	}
	
	@Override
	public String getSwitchLabel(boolean plural) {
		if (plural)
			return "packet-processing hosts";
		else
			return "packet-processing host";
	}
}
