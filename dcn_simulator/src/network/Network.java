package network;

import java.io.PrintStream;

/**
 * The root interface in the network hierarchy.
 * 
 * <p>Defines the methods to be implemented by Network classes in order to construct and operate the network.</p>
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public interface Network {
	
	/**
	* Returns a Node based on a given address.
	* @param address address of the Node to be returned.
	* @return Node with the specified address.
	*/
	public abstract Node getNode(final Address address);

	/**
	* Returns a Node corresponding to a given nodeID.
	* @param nodeID ID of the Node to be returned.
	* @return Node with the specified nodeID.
	*/
	public abstract Node getNode(int nodeID);

	/** @return the number of levels in the network */
	public abstract int  getNumLevels();

	/** @return the number of links between nodes in the network */
	public abstract int  getNumLinks();

	/** @return the number of nodes in the network */
	public abstract int  getNumNodes();

	/** @return the number of nodes in ON state in the network */
	public abstract int  getNumNodesON();

	/** @return the number of server resources in the network */
	public abstract int  getNumResources();

	/** @return the number of servers in the network */
	public abstract int  getNumServers();

	/** @return the number of switches in the network */
	public abstract int  getNumSwitches();

	/**
	 * Returns a Server corresponding to a given serverID.
	 * @param serverID ID of the Server to be returned.
	 * @return Node with the specified serverID.
	 */
	public abstract Node getServer(int serverID);

	/**
	 * Returns a Switch corresponding to a given switchID.
	 * @param switchID ID of the Switch to be returned.
	 * @return Node with the specified switchID.
	 */
	public abstract Node getSwitch(int switchID);

	/** 
	 * Returns a String representation to refer to Server components in the network
	 * @param plural indicates if the method should return a singular (<code>false</code>) 
	 * or plural <code>true</code> expression.
	 * @return String object containing the label 
	 */
	public abstract String getServerLabel(boolean plural);

	/** 
	 * Returns a String representation to refer to Switch components in the network
	 * @param plural indicates if the method should return a singular (<code>false</code>) 
	 * or plural <code>true</code> expression.
	 * @return String object containing the label 
	 */
	public abstract String getSwitchLabel(boolean plural);

	/**
	 * Returns the current power consumption of the network, based on the ON/OFF state of devices and their ports.
	 * @return network power consumption.
	 */
	public double powerConsumption();

	/**
	 * Prints the network in a comprehensive way depending on implementation
	 * @param out is a PrintStrem, where the network is to be printed, for example <code>System.out</code>
	 */
	public abstract void print(final PrintStream out);

	/**
	 * Resets the number of resources according to the actual size of the resources arrays of servers
	 */
	public abstract void resetNumResources();

	/**
	 * Resets the state of the network by reseting all its nodes
	 * @param state boolean indicating if the Nodes must be reset to ON (<code>true</code>) or OFF (<code>false</code>)
	 */
	public abstract void resetState(boolean state);

	/**
	 * Sets the number of resources for the network.
	 * <p>It is intended to reduce the number of resources originally assigned, 
	 * it does not make any change to the actual arrays of resources in Servers
	 * @param numResources new number of resources to consider
	 * @return <code>true</code> if the new number of resources is less than the original, 
	 * otherwise returns <code>false</code> and does make any change
	 */
	public abstract boolean setNumResources(int numResources);
}
