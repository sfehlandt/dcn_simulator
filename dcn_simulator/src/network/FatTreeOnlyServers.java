package network;


/**
 * Implementation of a FatTree network with only servers implementing the {@link Network} interface
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class FatTreeOnlyServers extends FatTree {
	
	//****************************** CONSTRUCTORS **********************************
	/**
     * Constructs a new FatTree network with only servers. 
     * @param netArgs    array of parameters to construct the network,  parsed as Strings.
     * @param serverArgs array of parameters to construct the servers,  parsed as Strings.
     */
	public FatTreeOnlyServers(final String[] netArgs, final String[] serverArgs) {
		super(netArgs, serverArgs, null);
   	}	

	//******************************** METHODS *************************************	
	@Override
	protected void createSwitchingDevices(){
		//---- Create Core Switches ----//
   		for (int o3 = 1; o3 <= halfPods; o3++)	   // for first dimension (top-down)			
   			for (int o4 = 1; o4 <= halfPods; o4++) // for second dimension (left-right)
   				coreSwitches.set(o3-1,o4-1, 
   					new Server(switchesCounter++, nodesCounter++, numPods, 3, 
   							serverParams, new Address(prefix,numPods,o3,o4), true));
   
   		//---- Create Pod Switches ----//
   		for (int pod = 0; pod < numPods; pod++)	 // for each pod			
   			for (int sw = 0; sw < numPods; sw++) // for each switch
   				podSwitches.set(pod,sw, 
   					new Server(switchesCounter++, nodesCounter++, numPods, (pod < halfPods ? 1 : 2), 
   							serverParams, new Address(prefix,pod,sw,1), true));
	}
	
	@Override
	public String getServerLabel(boolean plural) {
		if (plural)
			return "end-hosts";
		else
			return "end-host";
	}
	
	@Override
	public String getSwitchLabel(boolean plural) {
		if (plural)
			return "packet-processing hosts";
		else
			return "packet-processing host";
	}
}
