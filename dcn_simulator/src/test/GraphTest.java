package test;

import org.jgrapht.alg.*;
import org.jgrapht.graph.*;
import java.util.List;

/* Dijkstra Tutorial Exercise 3
 * 
 * 
 */
/**
 * Program used to test the library JGraphT, particularly the Dijkstra's algorithm
 * @author sfehland
 *
 */
public class GraphTest {
	
	/**
	 * Main method, executes the test
	 * @param args array of arguments, currently unused
	 */
    public static void main(String args[]) {

        SimpleWeightedGraph<String, DefaultWeightedEdge>  graph = 
        new SimpleWeightedGraph<String, DefaultWeightedEdge>
        (DefaultWeightedEdge.class); 
        
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addVertex("c");
        graph.addVertex("d");
        graph.addVertex("e");
        graph.addVertex("f");
        graph.addVertex("g");
        graph.addVertex("h");
        graph.addVertex("i");
        graph.addVertex("j");
        graph.addVertex("k");
        graph.addVertex("l");
        graph.addVertex("p");
        graph.addVertex("q");
        graph.addVertex("r");

        //---- a
        DefaultWeightedEdge e1 = graph.addEdge("a", "b"); 
        graph.setEdgeWeight(e1, 9); 

        DefaultWeightedEdge e2 = graph.addEdge("a", "l"); 
        graph.setEdgeWeight(e2, 7); 

        DefaultWeightedEdge e3 = graph.addEdge("a", "q"); 
        graph.setEdgeWeight(e3, 10); 

        //---- b
        DefaultWeightedEdge e4 = graph.addEdge("b", "k"); 
        graph.setEdgeWeight(e4, 7);
        
        DefaultWeightedEdge e5 = graph.addEdge("b", "q"); 
        graph.setEdgeWeight(e5, 5); 
        
        //---- c
        DefaultWeightedEdge e6 = graph.addEdge("c", "f"); 
        graph.setEdgeWeight(e6, 4); 
        
        DefaultWeightedEdge e7 = graph.addEdge("c", "j"); 
        graph.setEdgeWeight(e7, 3); 
        
        DefaultWeightedEdge e8 = graph.addEdge("c", "p"); 
        graph.setEdgeWeight(e8, 5); 
        
        DefaultWeightedEdge e9 = graph.addEdge("c", "r"); 
        graph.setEdgeWeight(e9, 9); 
        
        //---- d
        DefaultWeightedEdge e10 = graph.addEdge("d", "g"); 
        graph.setEdgeWeight(e10, 6);
        
        DefaultWeightedEdge e11 = graph.addEdge("d", "h"); 
        graph.setEdgeWeight(e11, 4); 
        
       //---- e
        DefaultWeightedEdge e12 = graph.addEdge("e", "f"); 
        graph.setEdgeWeight(e12, 6);
        
        DefaultWeightedEdge e13 = graph.addEdge("e", "r"); 
        graph.setEdgeWeight(e13, 6); 

        //---- f
        DefaultWeightedEdge e14 = graph.addEdge("f", "j"); 
        graph.setEdgeWeight(e14, 3);
        
        //---- g
        DefaultWeightedEdge e15 = graph.addEdge("g", "k"); 
        graph.setEdgeWeight(e15, 4); 
        
        DefaultWeightedEdge e16 = graph.addEdge("g", "p"); 
        graph.setEdgeWeight(e16, 6); 
        
        DefaultWeightedEdge e17 = graph.addEdge("g", "r"); 
        graph.setEdgeWeight(e17, 5); 
        
        //---- h
        DefaultWeightedEdge e18 = graph.addEdge("h", "r"); 
        graph.setEdgeWeight(e18, 3);
        
        //---- i
        DefaultWeightedEdge e19 = graph.addEdge("i", "l"); 
        graph.setEdgeWeight(e19, 2);
        
        DefaultWeightedEdge e20 = graph.addEdge("i", "p"); 
        graph.setEdgeWeight(e20, 3);
        
        DefaultWeightedEdge e21 = graph.addEdge("i", "q"); 
        graph.setEdgeWeight(e21, 4);
        
        //---- j
        DefaultWeightedEdge e22 = graph.addEdge("j", "l"); 
        graph.setEdgeWeight(e22, 9);
        
        //---- k
        DefaultWeightedEdge e23 = graph.addEdge("k", "q"); 
        graph.setEdgeWeight(e23, 7);
        
        //---- l
        //---- p
        DefaultWeightedEdge e24 = graph.addEdge("p", "q"); 
        graph.setEdgeWeight(e24, 3);
        
        DefaultWeightedEdge e25 = graph.addEdge("p", "r"); 
        graph.setEdgeWeight(e25, 7);      
        
        //---- q
        //---- r

        System.out.println("Shortest path from p to d:");
        DijkstraShortestPath<String,DefaultWeightedEdge> shortestPath = 
        		new DijkstraShortestPath<String,DefaultWeightedEdge>(graph, "p", "d");
        
        List<DefaultWeightedEdge> spEdges = shortestPath.getPathEdgeList();
        double pathLength = shortestPath.getPathLength();
        
        System.out.println(spEdges);        
        System.out.println("Path length = " + pathLength);
        
        //-------- Test reversed Edge-----------        
        DefaultWeightedEdge e26 = graph.addEdge("r", "p"); 
        //graph.setEdgeWeight(e26, 8);
        
        System.out.println("e25 = " + e25);
        System.out.println("e26 = " + e26);   
        
        System.out.println("graph.getEdge(p, r) = " + graph.getEdge("p", "r"));
        System.out.println("graph.getEdge(r, p) = " + graph.getEdge("r", "p"));
    }
}
