package util;


/**
 * Efficient implementation of 3D Matrix array template
 *
 * <p>It is more efficient memory-wise than regular multidimensional arrays.</p>
 *
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Array3D<T> {
	public int sizeX;
	public int sizeY;
	public int sizeZ;
	public T[] items;

	//******************************    Constructors    ****************************
	/**
	 * Construct a new Array3D
	 * @param x (int) number of elements in the 1st dimension.
	 * @param y (int) number of elements in the 2nd dimension.
	 * @param z (int) number of elements in the 3rd dimension.
	 */
	@SuppressWarnings("unchecked")
	public Array3D(int x, int y, int z) {
		sizeX = x;
		sizeY = y;
		sizeZ = z;
		items = (T[]) new Object[x*y*z];
	}
	
	//*******************************    Methods    ********************************
	private int find(int x, int y, int z){
		if      (x<0)       throw new IndexOutOfBoundsException("Invalid Array3D 1st coordinate: " + x +"< 0");
		else if (x>sizeX-1) throw new IndexOutOfBoundsException("Invalid Array3D 1st coordinate: " + x +">"+(sizeX-1));
		
		if      (y<0)       throw new IndexOutOfBoundsException("Invalid Array3D 2nd coordinate: " + y +"< 0");
		else if (y>sizeY-1) throw new IndexOutOfBoundsException("Invalid Array3D 2nd coordinate: " + y +">"+(sizeY-1));
		
		if      (z<0)       throw new IndexOutOfBoundsException("Invalid Array3D 3rd coordinate: " + z +"< 0");
		else if (z>sizeZ-1) throw new IndexOutOfBoundsException("Invalid Array3D 3rd coordinate: " + z +">"+(sizeZ-1));
		
		return x*sizeY*sizeZ + y*sizeZ + z;
	}

	/** Returns an element based on given coordinates 
	 * @param x (int) coordinate in the 1st dimension.
	 * @param y (int) coordinate in the 2nd dimension.
	 * @param z (int) coordinate in the 3rd dimension.
	 * @return the corresponding element
	 */
	public T get(int x, int y, int z) {
		return items[find(x,y,z)];
	}
	
	/** Assigns an given element to given coordinates 
	 * @param x (int) coordinate in the 1st dimension.
	 * @param y (int) coordinate in the 2nd dimension.
	 * @param z (int) coordinate in the 3rd dimension.
	 * @param value (T) element to be stored in the (x,y,z) location.
	 */
	public void set(int x, int y, int z, T value) {
		items[find(x,y,z)] = value;
	}
}
