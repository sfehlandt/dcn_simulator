package util;

/**
 * Efficient implementation of 2D Matrix array template
 *
 * <p>It is more efficient memory-wise than regular multidimensional arrays.</p>
 *
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Array2D<T>{
	public int sizeX;
	public int sizeY;
	public T[] items;

	//******************************    Constructors    ****************************
	/**
	 * Construct a new Array2D
	 * @param x (int) number of elements in the 1st dimension.
	 * @param y (int) number of elements in the 2nd dimension.
	 */
	@SuppressWarnings("unchecked")
	public Array2D(int x, int y) {
		sizeX = x;
		sizeY = y;
		items = (T[]) new Object[x*y];
	}
	
	//*******************************    Methods    ********************************
	private int find(int x, int y){
		if      (x<0)       throw new IndexOutOfBoundsException("Invalid Array2D 1st coordinate: " + x +"< 0");
		else if (x>sizeX-1) throw new IndexOutOfBoundsException("Invalid Array2D 1st coordinate: " + x +">"+(sizeX-1));
		
		if      (y<0)       throw new IndexOutOfBoundsException("Invalid Array2D 2nd coordinate: " + y +"< 0");
		else if (y>sizeY-1) throw new IndexOutOfBoundsException("Invalid Array2D 2nd coordinate: " + y +">"+(sizeY-1));
		
		return x*sizeY + y;
	}

	/** Returns an element based on given coordinates 
	 * @param x (int) coordinate in the 1st dimension.
	 * @param y (int) coordinate in the 2nd dimension.
	 * @return the corresponding element
	 */
	public T get(int x, int y) {
		return items[find(x,y)];
	}
	
	/** Assigns an given element to given coordinates 
	 * @param x (int) coordinate in the 1st dimension.
	 * @param y (int) coordinate in the 2nd dimension.
	 * @param value (T) element to be stored in the (x,y) location.
	 * @throws java.lang.IndexOutOfBoundsException
	 */
	public void set(int x, int y, T value) {
		items[find(x,y)] = value;
	}
}
