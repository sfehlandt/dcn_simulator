package util;

public class StaticTools {
	
	public static double mean(double[] v) {
		double tot = 0.0;
		for (int i = 0; i < v.length; i++)
			tot += v[i];
		return tot / v.length;
	}
	
	public static double mean(int[] v) {
		double tot = 0.0;
		for (int i = 0; i < v.length; i++)
			tot += v[i];
		return tot / v.length;
	}
	
	public static double mean(long[] v) {
		double tot = 0.0;
		for (int i = 0; i < v.length; i++)
			tot += v[i];
		return tot / v.length;
	}
	
	public static double variance(double[] v) {
        double mean = mean(v);
        double temp = 0;
        
        for (int i = 0; i < v.length; i++)
            temp += (v[i] - mean)*(v[i] - mean);
        
        return temp/v.length;
    }
	
	public static double variance(int[] v) {
        double mean = mean(v);
        double temp = 0;
        
        for (int i = 0; i < v.length; i++)
            temp += (v[i] - mean)*(v[i] - mean);
        
        return temp/v.length;
    }
	
	public static double variance(long[] v) {
        double mean = mean(v);
        double temp = 0;
        
        for (int i = 0; i < v.length; i++)
            temp += (v[i] - mean)*(v[i] - mean);
        
        return temp/v.length;
    }
	
	public static double stdDev(double[] v){
		return Math.sqrt(variance(v));
	}
	
	public static double stdDev(int[] v){
		return Math.sqrt(variance(v));
	}
	
	public static double stdDev(long[] v){
		return Math.sqrt(variance(v));
	}

}
