package user;

import java.io.IOException;

import sim.MRGRouting;

/**
 * Comparison 1:
 * FatTree, 8 pods, 3 resources
 * Single-resource vs Multi-resource algorithms
 * @author sfehland
 *
 */
public class R2FatTree {
	public static void main (String[] args) throws IOException {
		int[] numFlowsList = {20, 40, 60, 80, 100, 200, 300, 400, 800};
		int numSamples     = 20;              // number of samples
		int numResSim1     = 3;               // number of resources to limit simulation 1
		String networkFileName = "fattreeServers_results";
		
		// List of algorithms to use:
		int[] algsList     = {
				MRGRouting.SR_SPATH,
				MRGRouting.SR_GREEN,
				MRGRouting.MR_SPATH,
				MRGRouting.MR_GREEN,
				//MRGRouting.SR_SPATH_SAVE_RESOURCES,
				//MRGRouting.SR_GREEN_SAVE_RESOURCES,
				//MRGRouting.MR_SPATH_SAVE_RESOURCES,
				//MRGRouting.MR_GREEN_SAVE_RESOURCES
				};
		
		CompareSim csim = new CompareSim();
		csim.runSimulation1(numFlowsList, algsList, numSamples, networkFileName,  numResSim1);
	}
}
