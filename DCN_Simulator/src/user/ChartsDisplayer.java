package user;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.RefineryUtilities;

import sim.Simulator;

/**
 * GUI used to display 4 charts.
 * Includes a button "Save All", to save all the images.
 * @author Sebastian Fehlandt
 * @since Aug 2016
 */
public class ChartsDisplayer extends JFrame implements ActionListener, WindowListener {
	
	private static final long serialVersionUID = 1L;	
	private int width  = 480;           // width  of the image when saved
	private int height = 360;           // height of the image when saved
	private int frameWidth  = 560;      // width  of the frame
	private int frameHeight = 300;      // height of the frame
	private JFreeChart chart1;          // first  chart
	private JFreeChart chart2;          // second chart
	private JFreeChart chart3;          // third  chart
	private JFreeChart chart4;          // fourth chart
	private ChartPanel chartPanel1;     // panel to display chart1
	private ChartPanel chartPanel2;     // panel to display chart2
	private ChartPanel chartPanel3;     // panel to display chart3
	private ChartPanel chartPanel4;     // panel to display chart4
	private JPanel chartsPanel;         // panel containing char panels
	private JButton saveAllButton;      // button to save all the charts simultaneously
	private String chartFileName1 = "Chart1.png";
	private String chartFileName2 = "Chart2.png";
	private String chartFileName3 = "Chart3.png";
	private String chartFileName4 = "Chart4.png";
	
	/**
	 * Creates a new ChartsDisplayer
	 * @param applicationTitle title to display on the frame
	 * @param dir              directory to set as the default directory for saved images
	 */
	public ChartsDisplayer( String applicationTitle, String dir ){
		
		super( applicationTitle ); // call JFrame super constructor
		this.setLayout(new BorderLayout());		
		
		// Define window:
		chartsPanel = new JPanel();
		chartsPanel.setLayout(new GridLayout(2, 2));		
		this.add(chartsPanel, BorderLayout.CENTER);
		
		// Create save all button:
		saveAllButton = new JButton("Save All");
		this.add(saveAllButton, BorderLayout.SOUTH);
		saveAllButton.addActionListener(this);
		
		// Create ChartPanels:
		chartPanel1 = new ChartPanel( null );        
		chartPanel1.setPreferredSize(new java.awt.Dimension( frameWidth , frameHeight ) );
		chartPanel1.setDefaultDirectoryForSaveAs(new File(dir));
		chartsPanel.add( chartPanel1 );
		
		chartPanel2 = new ChartPanel( null );        
		chartPanel2.setPreferredSize(new java.awt.Dimension( frameWidth , frameHeight ) );
		chartPanel2.setDefaultDirectoryForSaveAs(new File(dir));
		chartsPanel.add( chartPanel2 );
		
		chartPanel3 = new ChartPanel( null );        
		chartPanel3.setPreferredSize(new java.awt.Dimension( frameWidth , frameHeight ) );
		chartPanel3.setDefaultDirectoryForSaveAs(new File(dir));
		chartsPanel.add( chartPanel3 );
		
		chartPanel4 = new ChartPanel( null );        
		chartPanel4.setPreferredSize(new java.awt.Dimension( frameWidth , frameHeight ) );
		chartPanel4.setDefaultDirectoryForSaveAs(new File(dir));
		chartsPanel.add( chartPanel4 );
		
		// Build window:
		this.pack( );        
		RefineryUtilities.centerFrameOnScreen( this );        
		this.setVisible( false );
		
		this.addWindowListener(this); // Add windows listener to control window buttons
	}
	
	/**
	 * Sets the charts to be displayed and displays the window
	 * @param chart1 chart to be displayed in upper-left  corner
	 * @param chart2 chart to be displayed in upper-right corner
	 * @param chart3 chart to be displayed in lower-left  corner
	 * @param chart4 chart to be displayed in lower-right corner
	 */
	public void setCharts(JFreeChart chart1, JFreeChart chart2, JFreeChart chart3, JFreeChart chart4) {
		this.chart1 = chart1;
		this.chart2 = chart2;
		this.chart3 = chart3;
		this.chart4 = chart4;
		
		chartPanel1.setChart(this.chart1);
		chartPanel2.setChart(this.chart2);
		chartPanel3.setChart(this.chart3);
		chartPanel4.setChart(this.chart4);
		setVisible(true);
	}
	
	/**
	 * Saves the charts into image files
	 * @throws IOException
	 */
	private void saveCharts() throws IOException{
		File fileChart1 = new File(Simulator.OUT_DIR, chartFileName1 );
		File fileChart2 = new File(Simulator.OUT_DIR, chartFileName2 ); 
		File fileChart3 = new File(Simulator.OUT_DIR, chartFileName3 ); 
		File fileChart4 = new File(Simulator.OUT_DIR, chartFileName4 ); 
		
		ChartUtilities.saveChartAsPNG( fileChart1 , chart1 , width , height );
		ChartUtilities.saveChartAsPNG( fileChart2 , chart2 , width , height );
		ChartUtilities.saveChartAsPNG( fileChart3 , chart3 , width , height );
		ChartUtilities.saveChartAsPNG( fileChart4 , chart4 , width , height );
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == saveAllButton)
			try {
				saveCharts();
				saveAllButton.setEnabled(false);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}

	@Override
	public void windowClosing(WindowEvent e) {
		setVisible(false);
		dispose();
	}
	
	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}
}
