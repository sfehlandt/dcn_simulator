package network;

/**
 * Set of parameters to construct and model a {@link Server}.
 * 
 * <p>It is given as reference to the {@link Server} objects. 
 * This enables memory savings in cases with several servers with equal parameters.</p>
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class ServerParameters {
	
	//******************************** ATTRIBUTES **********************************
	protected double delay            = 0;   // [micro-seconds] packet forwarding delay time
	protected double bandwidth        = 0;   // [Gb] max bandwidth of network ports
	protected double portPower        = 0;   // [W] power consumption per port
	protected double coreMaxPower     = 0;   // [W] power consumption of processing core used for routing
	protected double routingCoreUsage = 0.5; // [p.u. or numeric] ratio of core usage for routing
	protected int     numResources    = 1;   // number of resources
	protected int origNumResources    = 1;   // original number of resources
	
	//****************************** CONSTRUCTORS **********************************
	/** Construct a new set of ServerParameters with default values	 */
	public ServerParameters(){}
	
	/**
	 * Construct a new set of ServerParameters with given parameters
	 * @param args array of parameters to construct servers, parsed as Strings.
	 */
	public ServerParameters(final String[] args){
		if (args == null) return;
		delay            = Double.parseDouble(args[0]);
		bandwidth        = Double.parseDouble(args[1]);
		portPower        = Double.parseDouble(args[2]);
		coreMaxPower     = Double.parseDouble(args[3]);
		routingCoreUsage = Double.parseDouble(args[4]);
		numResources     = Integer.parseInt(  args[5]);
		
		if (numResources < 1)
			numResources = 1;
		
		origNumResources = numResources;
	}
	
	//******************************** METHODS *************************************
	/** @return the number of Server resources */
	public int getNumResources() { return this.numResources; }
	
	
	/**
	 * Calculates the power consumption of a given Server according to the power model described by 
	 * the parameters stored in this object
	 * @param sv Server to calculate its power consumption
	 * @return the current power consumption of the Server
	 */
	protected double powerConsumption(final Server sv){
		if (sv.isOn())
			return coreMaxPower*routingCoreUsage + portPower*sv.getNumOnPorts();
		else
			return 0;
	}
	
	/**
	 * Resets the number of resources to its original value
	 */
	public void resetNumResources() {
		this.numResources = this.origNumResources;
	}
	
	/**
	 * Sets the number of resources to consider in Servers
	 * @param numResources new number of resources to consider
	 * @return <code>true</code> if the new number of resources is bigger than its original value, 
	 * and therefore, bigger than the capacity of the array
	 */
	public boolean setNumResources(int numResources) {
		if (this.numResources == numResources || numResources < 0) return false;
		
		this.numResources = numResources;
		return true;
	}
	
	@Override
	public String toString(){
		String str =        "Delay = "+delay+" [us]"    +
	                 ", Port power = "+portPower+" [W]" +
				     ", Core max-power = "+coreMaxPower+" [W]" +
				     ", Routing core-usage = "+ (routingCoreUsage*100) +" [%]" +
				     ", Number of resources = "+ getNumResources();
		return str;
	}
}
