package network;

/**
 * Set of parameters to construct and model a {@link Switch}.
 * 
 * <p>It is given as reference to the {@link Switch} objects. 
 * This enables memory savings in cases with several switches with equal parameters.</p>
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class SwitchParameters {
	
	//******************************** ATTRIBUTES **********************************
	protected double delay     = 0; // [micro-seconds] packet forwarding delay time
	protected double bandwidth = 0; // [Gb] max bandwidth of network ports
	protected double portPower = 0; // [W] power consumption per port
	protected double basePower = 0; // [W] switch base power consumption
	protected boolean baseIsFactor = true;
	
	//****************************** CONSTRUCTORS **********************************
	/** Construct a new set of ServerParameters with default values	 */
	public SwitchParameters(){}             // Constructor with default values
	
	/**
	 * Construct a new set of ServerParameters with given parameters
	 * @param args array of parameters to construct servers, parsed as Strings.
	 */
	public SwitchParameters(final String[] args){ // Constructor with values in a String array
		if (args == null) return;
		delay     = Double.parseDouble(args[0]);
		bandwidth = Double.parseDouble(args[1]);
		portPower = Double.parseDouble(args[2]);
		
		String basePowerStr = args[3];
		
		if (basePowerStr.contains("%")) {
			baseIsFactor = true;
			basePowerStr = basePowerStr.substring(0, basePowerStr.length()-1);
			basePower = Double.parseDouble(basePowerStr)/100;
		} else {
			baseIsFactor = false;
			basePower = Double.parseDouble(basePowerStr);
		}
	}
	
	//******************************** METHODS *************************************
	/**
	 * Calculates the power consumption of a given Switch according to the power model described by 
	 * the parameters stored in this object
	 * @param sw Switch to calculate its power consumption
	 * @return the current power consumption of the Switch
	 */
	protected double powerConsumption(final Switch sw){
		if (sw.isOn()) {
			
			if (baseIsFactor)
				return basePower*sw.getNumPorts() + portPower*sw.getNumOnPorts();
			else
				return basePower + portPower*sw.getNumOnPorts();
			
		} else {
			return 0;
		}
	}
	
	@Override
	public String toString(){
		String str =        "Delay = "+delay+" [us]"    +
	                 ", Port power = "+portPower+" [W]" +
				     ", Base power = "+basePower+" [W]";
		return str;
	}
}
