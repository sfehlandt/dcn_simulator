package network;

/**
 * Represents a DCN Switch
 *
 * <p>It is inherited from {@link Node}.</p>
 *
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Switch extends Node {

	//******************************** ATTRIBUTES **********************************
	protected final int switchID;	
	private SwitchParameters params;

	//****************************** CONSTRUCTORS **********************************}	
	/**
	 * Constructs a new Switch
	 * @param switchID (int) ID or index of the Switch as Switch in its network.
     * @param nodeID   (int) ID or index of the Server as Node in its network.
	 * @param numPorts (int) number of communication ports.
	 * @param level    (int) level of the node in the network.
	 * @param params   set of parameters to model the Switch
	 * @param address  ({@link Address}) network address of the node.
	 */
	public Switch(int switchID, int nodeID, int numPorts, int level, SwitchParameters params, Address address) {
		super(nodeID, numPorts, level, address);            // call parent class Node constructor
		this.switchID = switchID;                           // switch ID
		if(params == null) params = new SwitchParameters(); // if no parameters use default
		this.params = params;                               // assign parameters
		resetState(true);
	}

	//******************************** METHODS *************************************
	/** @return the unique ID of the Switch (among Switches, not Nodes) */
	@Override
	public int getDeviceID() {return this.switchID;}
	
	@Override
	public String getLabel() {return "Switch";}
	
	@Override
	public String parametersToString() {return params.toString();}
	
	@Override
	public double powerConsumption()   {return this.params.powerConsumption(this);}
}
