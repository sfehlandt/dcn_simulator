package network;

/**
 * Represents a network address of the form o1.o2.o3.o4, e.g. 10.4.2.1
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Address {

	//******************************** ATTRIBUTES **********************************
	private int o1, o2, o3, o4;
	
	//****************************** CONSTRUCTORS **********************************
	/**
	 * Construct a new Address of the form o1.o2.o3.o4, e.g. 10.4.2.1
	 * @param o1 (int) first  octet of address.
	 * @param o2 (int) second octet of address.
	 * @param o3 (int) third  octet of address.
	 * @param o4 (int) fourth octet of address.
	 */
	public Address(int o1, int o2, int o3, int o4) {
   		this.o1 = o1;
   		this.o2 = o2;
   		this.o3 = o3;
   		this.o4 = o4;
   	}
	
	/**
	 * Construct a new Address of the form o1.o2.o3.o4, e.g. 10.4.2.1
	 * @param str String containing the address, fields are identified by dot (".") separators
	 */
	public Address(String str){
		String[] strings = (str.trim()).split("\\.");
		this.o1 = Integer.parseInt(strings[0]);
		this.o2 = Integer.parseInt(strings[1]);
		this.o3 = Integer.parseInt(strings[2]);
		this.o4 = Integer.parseInt(strings[3]);
	}
	
	//******************************** METHODS *************************************
	/** @return the first  octet of the address */
	public int getOctet1() {return o1;}
	
	/** @return the second octet of the address */
	public int getOctet2() {return o2;}
	
	/** @return the third  octet of the address */
	public int getOctet3() {return o3;}
	
	/** @return the fourth octet of the address */
	public int getOctet4() {return o4;}
	
	@Override
	public String toString(){
		return o1 + "." + o2 + "." + o3 + "." + o4;
	}

}
