package network;

import sim.Messenger;

/**
 * Auxiliary class with static fields and method buildNetwork, used to build a Network based on argument arrays
 * and identifying the architecture
 * @author sfehland
 * @since Jul 2016
 */
public class NetworkBuilder {
	public static final String ARCH_FAT_TREE    = "fattree";
	public static final String ARCH_FAT_TREE_OS = "fattreeservers";
	public static final String ARCH_BCUBE       = "bcube";
	public static final String ARCH_BCUBE_OS    = "bcubeservers";
	
	/**
     * Builds a network base on given parameters and architecture
     * @param netArgs    array of parameters to construct the network,  parsed as Strings.
     * @param serverArgs array of parameters to construct the servers,  parsed as Strings.
     * @param switchArgs array of parameters to construct the switches, parsed as Strings.
     * @param arch       String containing the architecture to use.
     * @param msn        {@link Messenger} object to send messages, warnings and errors.
     * @return the {@link Network}
     */
	public static Network buildNetwork(final String[] netArgs, final String[] serverArgs, final String[] switchArgs,
			String arch, Messenger msn){
		
		Network net = null;
		arch = arch.replaceAll(" ","");
		
		//---- Identify architecture and build network ----
		if (arch.equalsIgnoreCase(ARCH_FAT_TREE)){
			if (msn != null)
				msn.sendMessage("Building FatTree with " + netArgs[0] + " pods");					
			net = new FatTree(netArgs, serverArgs, switchArgs);
			
		} else if (arch.equalsIgnoreCase(ARCH_FAT_TREE_OS)){
			if (msn != null)
				msn.sendMessage("Building FatTree with only servers, with " + netArgs[0] + " pods");					
			net = new FatTreeOnlyServers(netArgs, serverArgs);
			
		} else if (arch.equalsIgnoreCase(ARCH_BCUBE)){			
			if (msn != null)
				msn.sendMessage("Building BCube level " +  netArgs[1] +" with " + netArgs[0] + " switch ports");			
			net = new BCube(netArgs, serverArgs, switchArgs);
			
		} else if (arch.equalsIgnoreCase(ARCH_BCUBE_OS)){			
			if (msn != null)
				msn.sendMessage("Building BCube with only servers of level " +  netArgs[1] +
						" with " + netArgs[0] + " switch ports");			
			net = new BCubeOnlyServers(netArgs, serverArgs);
		}
		return net;
	}
}
