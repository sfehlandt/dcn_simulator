package network;


/**
 * Represents a DCN Server
 *
 * <p>It is inherited from {@link Node}.</p>
 *
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Server extends Node {

	//******************************** ATTRIBUTES **********************************
	protected final int serverID;
	private float[] resources;
	private ServerParameters params;
	private double inactiveWeight;
	private boolean isSwitch = false;

	//****************************** CONSTRUCTORS **********************************
	/**
     * Constructs a new Server
     * @param serverID (int) ID or index of the Server as Server in its network.
     * @param nodeID   (int) ID or index of the Server as Node in its network.
     * @param numPorts (int) number of communication ports.
     * @param level    (int) level of the node in the network.
     * @param params   set of parameters to model the Server
     * @param address  ({@link Address}) network address of the node.
     */
	public Server(int serverID, int nodeID, int numPorts, int level, ServerParameters params, Address address) {
		super(nodeID, numPorts, level, address);            // call parent class Node constructor
		this.serverID = serverID;                           // assign serverID
		if(params == null) params = new ServerParameters(); // if no parameters use default
		this.params = params;                               // assign parameters		
		this.resources = new float[getNumResources()];      // create resources array
		resetState(true);
	}
	
	/**
     * Constructs a new Server
     * @param serverID (int) ID or index of the Server as Server in its network.
     * @param nodeID   (int) ID or index of the Server as Node in its network.
     * @param numPorts (int) number of communication ports.
     * @param level    (int) level of the node in the network.
     * @param params   set of parameters to model the Server
     * @param address  ({@link Address}) network address of the node.
     * @param isSwitch boolean parameter, if (<code>true</code>) 
     * the Server is only a packet-processor, not and end-host
     */
	public Server(int serverID, int nodeID, int numPorts, int level, ServerParameters params, Address address,
			boolean isSwitch) {
		
		this(serverID, nodeID, numPorts, level, params, address); // call other constructor
		this.isSwitch = true;
		
	}
	
	//******************************** METHODS *************************************
	@Override
	public double calculateNodeWeight(final float[] demands) {
		
		int numResources = getNumResources();
		inactiveWeight = numResources*(numResources - 1.0)/2.0 + 1.0;
		nodeWeight = 0.0;
			
		if (this.isOn()) {
			for (int i = 0; i < numResources; i++) {
				for (int j = i + 1; j < numResources; j++) {
					boolean inv1 = resources[i] > resources[j] && demands[i] < demands[j];
					boolean inv2 = resources[i] < resources[j] && demands[i] > demands[j];
						
					if (inv1 || inv2) nodeWeight += 1.0;
				}
			}
			
		} else {
			nodeWeight = inactiveWeight;
		}
		return nodeWeight;
	}
	
	@Override
	public boolean checkResources(final float[] demands) {
		int numResources = getNumResources();
		for (int i = 0; i < numResources; i++) {
			if (demands[i] > resources[i]) return false;
		}
		return true;
	}
	
	/** @return the unique ID of the Server (among Servers, not Nodes) */
	@Override
	public int getDeviceID()     {return this.serverID;}
	
	@Override
	public String getLabel() {
		if (isSwitch)
			return "Switching-Server";
		else
			return "Server";
	}
	
	/** @return the number of Server resources */
	@Override
	public int getNumResources() {return this.params.getNumResources();}
	
	@Override
	public String parametersToString() {return params.toString(); }
	
	@Override
	public double powerConsumption()   {return this.params.powerConsumption(this);}
	
	@Override
	public void resetState(boolean state) {
		super.resetState(state);
		
		for (int i = 0; i < params.origNumResources; i++)
			this.resources[i] = 1;
	}
	
	@Override
	public boolean revertResources(float[] usages) {
		float aux;
		boolean ret = true;
		int numResources = getNumResources();
		
		for (int i = 0; i <  numResources; i++) {
			aux = this.resources[i] + usages[i];
			if (aux > 1) {
				aux = 1;
				ret = false;
			}				
		}	
		return ret;
	}
	
	@Override
	public boolean useResources(final float[] usages) {
		int numResources = getNumResources();
		
		float[] aux = new float[params.origNumResources];
		
		for (int i = 0; i <  numResources; i++) {
			if (usages[i] <= this.resources[i]) {
				aux[i] = this.resources[i] - usages[i];
			} else {
				return false;
			}
		}		
		this.resources = aux;
		return true;
	}
}
