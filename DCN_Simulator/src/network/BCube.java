package network;

import java.io.PrintStream;
import java.util.Collection;

import util.Array2D;

/**
 * Implementation of a BCube network implementing the {@link Network} interface
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class BCube implements Network {
	
	//******************************** ATTRIBUTES **********************************
	protected int numPorts;                  // number of switch ports
	protected int maxLevel;                  // maximum level (starting from 0, numberOfLevels = maxLevel + 1)
	protected int prefix;                    // network address prefix
	protected int numServers;                // number of servers
	protected int numSwitches;               // number of switches
	protected int switchesPerLevel;	         // number of switches per level
	protected int numLinks = 0;              // number of links
	protected Node[]         servers;        // set of servers
	protected Array2D<Node> switches;        // set of switches
	protected ServerParameters serverParams; // parameters for servers
	protected SwitchParameters switchParams; // parameters for switches
	protected boolean printFull = true;      // decide if print full nodes or just address
	protected int nodesCounter    = 0;       // counter to generate unique nodeIDs   or indexes
	protected int serversCounter  = 0;       // counter to generate unique serverIDs or indexes
	protected int switchesCounter = 0;       // counter to generate unique switchIDs or indexes
	protected Collection<Node> nodes = null; // unused collection of nodes, only to show association in UML Lab
	
	//****************************** CONSTRUCTORS **********************************
	/**
     * Construct a new BCube network
     * @param netArgs    array of parameters to construct the network,  parsed as Strings.
     * @param serverArgs array of parameters to construct the servers,  parsed as Strings.
     * @param switchArgs array of parameters to construct the switches, parsed as Strings.
     */
	public BCube(final String[] netArgs, final String[] serverArgs, final String[] switchArgs) {
		numPorts           = Integer.parseInt(netArgs[0]);     // netArgs[0]: number of ports for switches
		maxLevel           = Integer.parseInt(netArgs[1]);     // netArgs[1]: max levels
   		prefix             = Integer.parseInt(netArgs[2]);     // netArgs[2]: network prefix
   		serverParams       = new ServerParameters(serverArgs); // serverArgs: server parameters
   		switchParams       = new SwitchParameters(switchArgs); // switchArgs: switch parameters
   		
   		int n = numPorts; // BCube n parameter
   		int k = maxLevel; // BCube_k
   		
   		//---- Initialise servers and switches arrays ----//   		 
   		switchesPerLevel = (int) Math.pow(n, k);
   		numServers       = n * switchesPerLevel;
   		numSwitches      = switchesPerLevel * (maxLevel + 1); 
   		servers  = new Node[numServers];
   		switches = new Array2D<Node>(k+1 , switchesPerLevel);
   		
   		createDevices();
   		
   		//---- Connect servers to switches ----//   		
   		for (int l = 0; l <= k; l++) {  //---------------------------- for each level l = 0 to k
   			
   			int switchesPerBCube = (int) Math.pow(n, l);	// number of switches in a BCube_l
   			int serversPerBCube  = switchesPerBCube * n;	// number of servers  in a BCube_l
   			int bCubesPerLevel   = (int) Math.pow(n, k-l);	// number of BCube_l per level l
   			int sv = 0;				// server index
   			int startServer = 0;	// index of the first server of the current BCube
   			int startSwitch = 0;	// index of the first switch of the current BCube
   			
   			for (int bCube = 0; bCube < bCubesPerLevel; bCube++){
   				
   				for (int sw = 0; sw < switchesPerBCube; sw++){	//---- for each switch sw = 0 to n^k-1
   					sv = startServer + sw;
   	   				for (int port = 0; port < n; port++){		//---- for each port = 0 to n-1
   	   					
   	   					servers[sv].connect(l, port, switches.get(l,startSwitch + sw)); // server -> switch
   	   					switches.get(l,startSwitch + sw).connect(port, l, servers[sv]); // switch -> server
   	   					sv += switchesPerBCube;                                         // jump to next server
   	   					numLinks++;
   	   				}
   	   			}
   				startSwitch += switchesPerBCube; // Move to first switch of next BCube
   				startServer += serversPerBCube;  // Move to first server of next BCube
   			}
   		}
	}
	
	protected void createDevices(){
		int n = numPorts; // BCube n parameter
   		int k = maxLevel; // BCube_k
		
		//---- Create switches ----//
   		for (int level = 0; level <= k; level++) {          // for each level
   			for (int sw = 0; sw < switchesPerLevel; sw++) { // for each switch
   				switches.set(level,sw,new Switch(switchesCounter++, nodesCounter++, n, level, switchParams, 
   						new Address(prefix,level,sw,n)));
   			}
   		}
   		
   		//---- Create servers ----//
   		int i = 0;
   		for (int sw = 0; sw < switchesPerLevel; sw++){	// for each switch
   			for (int sv = 0; sv < n; sv++){				// for each server
   				servers[i] = new Server(serversCounter++, nodesCounter++, k+1, 0, serverParams, 
   						new Address(prefix,0,sw,sv));
   				i++;
   			}   			
   		}
	}
	
	//******************************** METHODS *************************************
	@Override
	public int getNumLevels() {return maxLevel + 1;}
	
	@Override
	public int getNumLinks()  {return numLinks;}
	
	@Override
	public int getNumNodes()  {return getNumSwitches() + getNumServers();}
	
	@Override
	public int getNumNodesON() {
		int counter = 0;
		
		//---- Check switches ----//
   		for (int level = 0; level <= maxLevel; level++) {    // for each level
   			for (int sw = 0; sw < switchesPerLevel; sw++) {  // for each switch
   				if (switches.get(level,sw).isOn()) counter++;
   			}
   		}
   		
   		//---- Check servers ----//
   		for (int i = 0; i < numServers; i++) {    // for each server
   			if (servers[i].isOn()) counter++;
   		}
   		
		return counter;
	}
	
	@Override
	public int getNumResources(){ return serverParams.getNumResources();}
	
	@Override
	public int getNumServers() {return numServers;}
	
	@Override
	public int getNumSwitches() {return numSwitches;}
	
	@Override
	public Node getNode(final Address address) {
		int o2 = address.getOctet2();
		int o3 = address.getOctet3();
		int o4 = address.getOctet4();
		
		if (o4 == numPorts) return switches.get(o2,o3);  // it is a switch		
		else return switches.get(o2,o3).getAdjacentNode(o4); // it is a server
	}
	
	@Override
	public Node getNode(int nodeID) {
				
		Node node = null;
		int numSwitches = getNumSwitches();
		int numServers  = getNumServers();
		int numNodes    = numSwitches + numServers;
		
		if (nodeID < numSwitches){    // The first numSwitches nodes are switches
			node = getSwitch(nodeID);
		
		}else if (nodeID < numNodes){ // Then we have numServers servers
			node = servers[nodeID - numSwitches];
			
		}else{
			throw new IndexOutOfBoundsException("nodeID " + nodeID + " is out of bounds, maxNodeID = " + (numNodes-1));
		}
		
		if (node.getNodeID() == nodeID)
			return node;
		else
			throw new RuntimeException("Error searching nodeID " + nodeID + 
					                    ", node found has ID = " + node.getNodeID());
	}
	
	@Override
	public Node getServer(int serverID) {
				
		Node server = null;
		int numSwitches = getNumSwitches();
		int numServers  = getNumServers();
		int numNodes    = numSwitches + numServers;
		
		if (serverID < numNodes){ // Then we have numServers servers
			server = servers[serverID];
			
		}else{
			throw new 
			IndexOutOfBoundsException("serverID " + serverID + " is out of bounds, maxServerID = " + (numNodes-1));
		}
		
		if (server.getDeviceID() == serverID)
			return server;
		else
			throw new 
			RuntimeException("Error searching serverID " + serverID + ", server found has ID = "+server.getDeviceID());
	}
	
	@Override
	public Node getSwitch(int switchID) {
				
		Node swt = null;
		int numSwitches = getNumSwitches();
		
		if (switchID < numSwitches){ // The first numSwitches nodes are switches
			int x = switchID / switchesPerLevel;
			int y = switchID % switchesPerLevel;
			swt = switches.get(x, y);
		
		}else{
			throw new 
			IndexOutOfBoundsException("switchID " + switchID + " is out of bounds, maxSwitchID = " + (numSwitches-1));
		}
		
		if (swt.getDeviceID() == switchID)
			return swt;
		else
			throw new 
			RuntimeException("Error searching switchID " + switchID + ", swicth found has ID = " + swt.getDeviceID());
	}

	@Override
	public String getServerLabel(boolean plural) {
		if (plural)
			return "end-hosts";
		else
			return "end-host";
	}
	
	@Override
	public String getSwitchLabel(boolean plural) {
		if (plural)
			return "switches";
		else
			return "switch";
	}
	
	@Override
	public double powerConsumption() {
		double power = 0;
		
		//---- Check switches ----//
   		for (int level = 0; level <= maxLevel; level++) {    // for each level
   			for (int sw = 0; sw < switchesPerLevel; sw++) {  // for each switch
   				power += switches.get(level,sw).powerConsumption();
   			}
   		}
   		
   		//---- Check servers ----//
   		for (int i = 0; i < numServers; i++)               // for each server
   			power += servers[i].powerConsumption();
   		
		return power;
	}
	
	@Override
	public void print(final PrintStream out) {
		
		out.println("******************************** Addresses ************************************************");
		//---- Print Switches ----//
		for (int l = maxLevel; l >= 0; l--){	//------------------------ for each level in backwards order
			
			out.println("Level " + l + " switches: "); 					// print level number
			
			for (int sw = 0; sw < switchesPerLevel; sw++) {	//------------ for each switch
				out.println(sw + ":\t" + switches.get(l,sw).toString(printFull));	// print switch
			}
			out.println("");
		}
		
		//---- Print Servers ----//
		out.println("Servers: ");
		for (int sv = 0; sv < numServers; sv++){	//---- for each server
			out.println(sv + ":\t" + servers[sv].toString(printFull)); // print server
		}
		out.println("");
		
		out.println("******************************** Server Number ********************************************");
		int maxDigits = String.valueOf(numServers).length();
				
		for (int l = maxLevel; l >= 0; l--){				//-------- for each level in backwards order
					
			out.println("Level " + l + " switches: ");
			
			for (int sw = 0; sw < switchesPerLevel; sw++){	//-------- for each switch
				out.print(sw + ":\t | "); 							// print switch number
				
				for (int port = 0; port < numPorts; port++){//-------- for each port
					
					// get connected server's serverID and convert to String with equal length (filled with blanks):
					int serverID = ((Server)switches.get(l,sw).getAdjacentNode(port)).getDeviceID();
					int numSpaces = maxDigits - String.valueOf(serverID).length();
					String spaces = "";
					for (int i = 0; i < numSpaces; i++)	spaces += " ";
					
					// print Port -> Server association:
					out.print("Port "+port+" -> Server " + spaces + serverID + " | ");
				}
				out.println("");
			}
			out.println("");
		}
	}
	
	@Override
	public void resetNumResources() {
		this.serverParams.resetNumResources();
	}

	@Override
	public void resetState(boolean state) {
		
		//---- Reset Switches ----//
		for (int l = maxLevel; l >= 0; l--) {               // for each level in backwards order			
			for (int sw = 0; sw < switchesPerLevel; sw++) { // for each switch
				switches.get(l,sw).resetState(state);       // reset Switch
			}
		}
		
		//---- Reset Servers ----//
		for (int sv = 0; sv < numServers; sv++) {           // for each server
			servers[sv].resetState(state);                  // print server
		}
	}
	
	@Override
	public boolean setNumResources(int numResources){
		return this.serverParams.setNumResources(numResources);
	}
}
