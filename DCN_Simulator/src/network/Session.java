package network;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.uncommons.maths.random.GaussianGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;
/**
 * Represents a Simulation Session
 * 
 * <p>It is represented by a Network and a set of flows that represent the network traffic</p>
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */

public class Session {
	
	//******************************** ATTRIBUTES **********************************
	// Network attributes:
	private Network net;
	private boolean initialNetState;
	
	// Flow attributes:
	private int numFlows = 0;
	private int numRoutedFlows  = 0;
	private int numAbortedFlows = 0;
	private float duration;
	private int staticDuration = 10;
	private List<Flow> flows;

	// Attributes for random number generation:
	private float normalMean;
	private float normalStdDev;
	private Random rand = null;
	private GaussianGenerator gauss = null;
	
	//****************************** CONSTRUCTOR ***********************************
	/**
     * Constructs a new Session.
     * @param net Network of the Session
     * @param initialState initial or default state for the {@link Network} in the Session, 
     * ON (<code>true</code>) or OFF (<code>false</code>)
     */
	public Session(Network net, boolean initialState){
		this.net             = net;
		this.initialNetState = initialState;
		this.net.resetState(initialNetState);
		this.rand  = new MersenneTwisterRNG();
	}
	
	//******************************** METHODS *************************************
	/**
     * Sets the route of the flow
     * @param flow {@link Flow} to be routed
     */
	public void abortFlow(Flow flow){
		flow.abortRouting();
		numAbortedFlows++;
	}
	
	/**
     * Generates a random traffic with normal distribution for a static simulation
     * @param numFlows the number of flows
     * @param mean mean of the normal distribution
     * @param stdDev standard deviation of the normal distribution
     */
	public void genNormalStaticTraffic(int numFlows, float mean, float stdDev){
		genNormalTraffic(numFlows, mean, stdDev, 0);
	}
	
	/**
     * Generates a random traffic with normal distribution for a static simulation
     * @param numFlows the number of flows
     * @param mean mean of the normal distribution
     * @param stdDev standard deviation of the normal distribution
     * @param duration time after which the simulation ends 
     */
	public void genNormalTraffic(int numFlows, float mean, float stdDev, float duration){
		
		// Check if the parameters have changed from last execution
		// If they have, we have to create a new GaussianGenerator with new parameters:
		if (gauss == null || this.normalMean != mean || this.normalStdDev != stdDev){
			this.normalMean   = mean;
			this.normalStdDev = stdDev;
			gauss = new GaussianGenerator(this.normalMean, this.normalStdDev, rand);
		}		
		
		// if duration is 0 we are in a static case:
		if (duration == 0)
			duration = staticDuration;
		
		this.numFlows     = numFlows;          // store number of flows
		this.duration     = duration;          // store duration	
		this.normalMean   = mean;              // store mean for Normal Distribution
		this.normalStdDev = stdDev;            // store standard deviation for Normal Distribution
		genTraffic();                          // generate the traffic
	}
	
	/** @return a list with all the demand values, in order to analyse its statistical distribution	 */
	public double[] getArrayRandNums() {
		int numResources = net.getNumResources();
		double[] array = new double[numResources * numFlows];
		int index = 0;
		
		for (Flow flow : flows){
			for (int i = 0; i < numResources; i++){
				array[index] = (double) flow.getDemand(i);
				index++;
			}
		}		
		return array;
	}
	
	/** @return a Set containing all the {@link Flow}s whose status is equal to {@link Flow.RoutingStatus#ABORTED} */
	public Set<Flow> getAbortedFlows() {
		Set<Flow> abortedFlows = new HashSet<Flow>(numAbortedFlows);
		
		for (Flow flow : flows) {
			if (flow.getStatus() == Flow.RoutingStatus.ABORTED) {
				abortedFlows.add(flow);
			}
		}		
		return abortedFlows;
	}
	
	/** @return the duration of the simulation session */
	public float getDuration() {return this.duration;}
	
	/** @return the {@link List} of {@link Flow}s being used by the session */
	public List<Flow> getFlows() {return this.flows;}
	
	/** @return the {@link Network} being used by the session */
	public Network getNetwork() {return this.net;}
	
	/** @return number of flows in the session */
	public int getNumFlows() {return this.numFlows;}
	
	/** @return number of nodes in the session's network */
	public int getNumNodes() {return this.net.getNumNodes();}
	
	/** @return the number of nodes in ON state in the session's network */
	public int getNumNodesON() {return this.net.getNumNodesON();}
	
	/** 
	 * Returns the number of congested nodes in the session's network.
	 * <p>A congested Node is a Node for which at least 1 resource is saturated
	 * @return the number of congested nodes in the network
	 */
	public int getNumNodesCongested() {
		
		int congestedCounter = 0;
				
		for (int i = 0; i < net.getNumServers(); i++) { //---- for each Node (server)
			Node node = net.getServer(i);
			if (!node.isOn()) continue;                 // if it is OFF we skip it
				
			boolean isCongested = false;
			
			for (Flow flow : flows) {                   //---- for each flow (pending or aborted)
				if (flow.getStatus() == Flow.RoutingStatus.ROUTED) continue; // if routed we skip it					
				
				isCongested = true;
				if (flow.checkNode(node)){  // if any flow can be routed we stop checking flows
					isCongested = false;
					break;
				}
			}
			
			if (isCongested)  // if the node can not route any of the pending flows we count it.
				congestedCounter++;
		}
		
		return congestedCounter;
	}
	
	/** @return number of flows currently routed */
	public int getNumRoutedFlows() {return this.numRoutedFlows;}
	
	/**
     * Returns the current power consumption of the Network of the Session
     * @see Network#powerConsumption
     * @return network power consumption.
     */
	public double getPowerConsumption() {return net.powerConsumption();}
	
	/** @return <code>true</code> if all the flows are routed, <code>false</code> if not */
	public boolean isFullyRouted() {return numFlows == numRoutedFlows + numAbortedFlows;}
	
	/**
     * Prints all the flows or requests
     * @param out {@link PrintStream} to print the flows, e.g. System.out
     */
	public void printRequests(PrintStream out){
		for(Flow flow : flows)
			out.println(flow.toString());
	}
	
	/** Regenerates the traffic for the session and resets its state */
	public void regenerateTraffic(){ 
		genTraffic();
		resetState(initialNetState);
	}
	
	/** Resets number of resources of the network and demands on the flows to their original values */
	public void resetNumResources() {
		this.net.resetNumResources();
		
		for(Flow flow : this.flows)
			flow.resetNumDemands();
	}
	
	/** Resets the state of the Session to its initial network state */
	public void resetState() {resetState(initialNetState);}
	
	/**
     * Resets the state of the Session by reseting the network and flow routes
     * @param state boolean indicating if the Nodes must be reset to ON (<code>true</code>) or OFF (<code>false</code>)
     */
	public void resetState(boolean state){

		this.net.resetState(state);
		this.numRoutedFlows  = 0;
		this.numAbortedFlows = 0;
		
		if (this.flows != null) {
			for(Flow flow : this.flows) {
				flow.deleteRoute();
			}
		}
	}
	
	/**
     * Sets the route of the flow
     * @param flow {@link Flow} to be routed
     * @param route list of {@link Node}s representing the route of the Flow in the Network
     * @return <code>true</code> if routing is successful, <code>false</code> if not
     */
	public boolean routeFlow(Flow flow, List<Node> route){
		if( flow.setRoute(route) ){
			numRoutedFlows++;
			return true;
		}
		else return false;
	}
	
	/**
	 * Sets the number of resources for the network and demands for the flows.
	 * <p>It is intended to reduce the number of resources originally assigned, 
	 * it does not make any change to the actual arrays of resources in Servers
	 * @param numResources new number of resources to consider
	 * @return <code>true</code> if the new number of resources is less than the original, 
	 * otherwise returns <code>false</code> and does make any change
	 */
	public boolean setNumResources(int numResources) {
		
		boolean ret = net.setNumResources(numResources);		
		if (!ret) return false;
		
		if (flows != null) {
			for (Flow flow : flows)
				ret = ret && flow.setNumDemands(numResources);
		}
		return ret;
	}
	
	//************************** PRIVATE AUXILIARY METHODS *************************
	/** 
	 * Generates an array of demands with random values based on the statistical parameters for their generation 
	 * @return array of float values representing the demands 
	 * */
	private float[] genDemands(){
		float[] demands = new float[net.getNumResources()];

		for (int i = 0; i < demands.length; i++){
			float num = 0;
			
			do {
				Double value = gauss.nextValue();
				num = (float) ((double) value);
			} while (num > 1 || num < 0);
			
			demands[i] = num;
		}
		
		return demands;
	}
	
	/** Generates traffic according to the stored parameters for flow generation, number of flows and duration */
	private void genTraffic(){
		this.flows = new ArrayList<Flow>(this.numFlows);
		this.numRoutedFlows = 0;
		int numServers = net.getNumServers();
		int srcIndex = -1;
		int tgtIndex = -1;
		
		for (int i = 0; i < numFlows; i++){
			srcIndex     = rand.nextInt(numServers);
			do {
				tgtIndex = rand.nextInt(numServers);
			} while (srcIndex == tgtIndex);
			
			Server src = (Server) net.getServer(srcIndex);
			Server tgt = (Server) net.getServer(tgtIndex);
			float[] demands = genDemands();
			Flow flow = new Flow(src, tgt, 0, duration, demands);
			flows.add(flow);
		}
		resetState(initialNetState);
	}
}
