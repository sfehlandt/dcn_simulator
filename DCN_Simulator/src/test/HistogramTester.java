package test;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.SimpleHistogramBin;
import org.jfree.data.statistics.SimpleHistogramDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import sim.MRGRouting;
import sim.Messenger;
import sim.PrintStreamMessenger;
import sim.RoutingAlgorithm;
import sim.Simulator;

/**
* A program to executes a simulation and plot the random numbers used for flow demands in an histogram
* @author Sebastian Fehlandt
* @since Aug 2016
*/
public class HistogramTester extends ApplicationFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Messenger msn;
	private String title;

	/**
     * Creates a new demo.     * 
     * @param title the frame title.
     * @throws IOException 
     */
    public HistogramTester(String title) throws IOException {
        super(title);
        this.title = title;
        
        JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
        
        this.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	             System.exit(0);
	          }        
	       });
    }
    
    /**
     * Creates a sample {@link HistogramDataset}.
     * 
     * @return the {@link IntervalXYDataset}.
     * @throws IOException 
     */
    private IntervalXYDataset createDataset() throws IOException {
    	
    	double[] obsArr = getArray();
    	int length      = obsArr.length;    	
    	msn.sendMessage("Number of values = "+length);
    	
    	double min = 99999999;
    	double max = -min;
    	
    	for (int i = 0; i < length; i++){    		
    		if (obsArr[i] > max) max = obsArr[i];
    		if (obsArr[i] < min) min = obsArr[i];
    	}
    	
        SimpleHistogramDataset dataset = new SimpleHistogramDataset("Series 1");
        
        final double increase = 0.001;
        double i1 = min;
        double i2 = 0;
        
        while (i1 < max){
        	i2 = i1 + increase;
        	dataset.addBin(new SimpleHistogramBin(i1, i2, true, false));
        	i1 = i2;
        }
        
        dataset.addObservations(obsArr);
        return dataset;     
    }
    
    /**
     * Creates a chart.
     * @param dataset a dataset of type {@link IntervalXYDataset}. 
     * @return The chart.
     */
    private JFreeChart createChart(IntervalXYDataset dataset) {
        JFreeChart chart = ChartFactory.createHistogram(
                title, null, null, dataset, 
                PlotOrientation.VERTICAL, true, true, false);
        return chart;
    }
        
    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     * @return A panel.
     * @throws IOException 
     */
    public JPanel createDemoPanel() throws IOException {
        JFreeChart chart = createChart(createDataset());
        return new ChartPanel(chart);
    }
    
    /**
     * Executes simulation and get list of random values
     * @return array of <code>double</code> containing the random values used to generate resource demands
     * @throws IOException
     */
    public double[] getArray() throws IOException{
    	int alg1  = MRGRouting.MR_GREEN;
		
		int verbosity = RoutingAlgorithm.VERBOSITY_ONE_LINE;
		
		float mean   = (float) 0.02;
		float stdDev = (float) 0.02;
		
		int numFlows = 500;
		
		msn = new PrintStreamMessenger();
		Simulator simulator = new Simulator(msn);
		simulator.setRoutingAlgorithm(new MRGRouting());
		
		
		//---------------------------- Execution 1 --------------------------
		simulator.buildNetwork("fattreeServers_results");
		simulator.generateStaticTraffic(numFlows, mean, stdDev);				
		simulator.runSimulation(alg1, verbosity);
		
		return simulator.getSession().getArrayRandNums();
    }
    
    /**
     * The starting point for the demo. 
     * @param args  ignored.
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        HistogramTester demo = new HistogramTester(
                "Distribution of Random Values");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}
