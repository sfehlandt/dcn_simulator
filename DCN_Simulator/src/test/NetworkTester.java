package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import network.Address;
import network.BCube;
import network.BCubeOnlyServers;
import network.FatTree;
import network.FatTreeOnlyServers;
import network.Network;
import network.Node;

/**
 * Tests different {@link Network} implementations
 * @author sfehland
 *
 */
public class NetworkTester {
	
	/**
	 * Returns an array with the parameters to construct a {@link FatTree}
	 * @param args array or arguments
	 * @param in   {@link BufferedReader} to read the parameters typed by the user.
	 * @return the array of parameters
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static String[] getFatTreeInput(String args[], BufferedReader in) throws NumberFormatException, IOException{
		
		System.out.print("Enter an even number of pods: ");
		int numPods = Integer.parseInt(in.readLine());
		
		int prefix = 10;
		args = new String[4];
		args[0] = numPods + "";
		args[1] = prefix  + "";
		
		return args;
	}
	
	/**
	 * Returns an array with the parameters to construct a {@link BCube}
	 * @param args array or arguments
	 * @param in   {@link BufferedReader} to read the parameters typed by the user.
	 * @return the array of parameters
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static String[] getBCubeInput(String args[], BufferedReader in) throws NumberFormatException, IOException{
		
		System.out.print("Enter BCube 'n' parameter (number of switch ports): ");
		int n = Integer.parseInt(in.readLine());
		
		System.out.print("Enter BCube 'k' parameter (max level, or BCube_k):  ");
		int k = Integer.parseInt(in.readLine());
		
		int prefix = 10;
		args = new String[5];
		args[0] = n + "";
		args[1] = k + "";
		args[2] = prefix  + "";
		
		return args;
	}
	
	/**
	 * Executes the test
	 * @param args array of arguments (currently unused)
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static void test(String args[]) throws NumberFormatException, IOException{
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		boolean repeat = true;
		Network net = null;
		
		//--------------- Choose and Build Network ---------------
		while (repeat == true){
			System.out.println("Choose a Network architecture to test (0 to exit): ");
			System.out.println("1. FatTree");
			System.out.println("2. BCube");
			System.out.println("3. FatTreeOnlyServers");
			System.out.println("4. BCubeOnlyServers");
			
			int choice = Integer.parseInt(in.readLine());
		
			switch (choice) {
				case 0:	 System.out.println("Finished Execution");
						 return;
			
				case 1:	 args = getFatTreeInput(args, in);
				         net = new FatTree(args, null, null);
				         net = new FatTree(args, null, null);
						 repeat = false;
						 break;
                 
				case 2:	 args = getBCubeInput(args, in);
				         net = new BCube(args, null, null);
				         net = new BCube(args, null, null);
						 repeat = false;
                		 break;
                		 
				case 3:  args = getFatTreeInput(args, in);
				         net = new FatTreeOnlyServers(args, null);
				         net = new FatTreeOnlyServers(args, null);
				         repeat = false;
				         break;
				
				case 4:  args = getBCubeInput(args, in);
		                 net = new BCubeOnlyServers(args, null);
		                 net = new BCubeOnlyServers(args, null);
		                 repeat = false;
		                 break;
                 
				default: System.out.println("Invalid number, try again");						 
						 break;
			}
		}
		
		//--------------- Print Network ---------------
		System.out.println("Network built with "+
				net.getNumSwitches()+ " " +net.getSwitchLabel(true) + " and " +
				net.getNumServers() + " " +net.getServerLabel(true));
		
		System.out.print("Do you want to print? (Y/N) ");
		String printString = in.readLine();
		char printChar = printString.trim().charAt(0);
		
		if (printChar == 'y' || printChar == 'Y')	
			net.print(System.out);
		
		//----------- Test getNode() by ID -----------
		System.out.println("");
		int numNodes = net.getNumServers() + net.getNumSwitches();
		boolean searchError = false;
		int i = 0;
		for(i = 0; i < numNodes; i++){
			Node node = net.getNode(i);
			if (node == null){
				System.out.println("Problem finding node " + i);
				searchError = true;
			}
		}
		if (!searchError) 
			System.out.println("  getNode(int   nodeID) is OK, all the "+i+" nodes where correctly found.");
		
		//----------- Test getSwitch() by ID -----------
		int numSwitches = net.getNumSwitches();
		searchError = false;
		for(i = 0; i < numSwitches; i++){
			Node swt = net.getSwitch(i);
			if (swt == null){
				System.out.println("Problem finding switch " + i);
				searchError = true;
			}
		}
		if (!searchError) 
			System.out.println("getSwitch(int switchID) is OK, all the "+i+" switches where correctly found.");
		
		//----------- Test getServer() by ID -----------
		int numServers = net.getNumServers();
		searchError = false;
		for(i = 0; i < numServers; i++){
			Node server = net.getServer(i);
			if (server == null){
				System.out.println("Problem finding server " + i);
				searchError = true;
			}
		}
		if (!searchError) 
			System.out.println("getServer(int serverID) is OK, all the "+i+" servers where correctly found.");
		
		//--------------- Search Individual Nodes ---------------
		while (true){
			System.out.print("\nEnter an address or nodeID to search (invalid or empty address to stop): ");
			String line = in.readLine();
			Address address = null;
			int index = -1;
			boolean invalidAddress = false;
			boolean invalidIndex   = false;
			Node node = null;
			
			try{
				address = new Address(line);
			} catch(Exception e){
				invalidAddress = true;
			}
			
			try{
				index = Integer.parseInt(line);
			} catch(Exception e){
				invalidIndex = true;
			}
					
			if (invalidAddress && invalidIndex){
				System.out.print("Invalid address and nodeID, finished execution ");
				return;
				
			}else if (!invalidAddress){
				System.out.println("Searching node by address: " + address);
				node = net.getNode(address);
			
			}else if (!invalidIndex){
				System.out.println("Searching node by nodeID: " + index);
				node = net.getNode(index);
			}
			
			System.out.println("Node found, nodeID = " + node.getNodeID());
			System.out.println(node.toString(true));
		}
	}
	
	/**
	 * Main method. Calls the {@link NetworkTester#test} method
	 * @param args array of arguments (currently unused)
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static void main(String[] args) throws NumberFormatException, IOException {
		test(args);
	}
}
