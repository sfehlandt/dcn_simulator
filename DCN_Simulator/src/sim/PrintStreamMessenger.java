package sim;

import java.io.PrintStream;

/**
 * Defines methods for printing output messages, warnings and errors to the 
 * <code>System.out</code> {@link PrintStream}
 * 
 * <p> Implements the {@link Messenger} interface
 *  
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */

public class PrintStreamMessenger implements Messenger {
	
	// Message labels and line tabulation for messages:
	public static final String ERROR_LABEL       = "ERROR! ";
	public static final String FATAL_ERROR_LABEL = "FATAL ERROR! ";
	public static final String WARNING_LABEL     = "WARNING! ";
	public static final String lineTab           = "- ";
	private boolean sameLine = false;
	
	
	protected PrintStream out;
	
	/**
	 * Constructs a new PrintStreamMessenger using {@link System#out}
     */
	public PrintStreamMessenger(){
		this.out = System.out;
	}
	
	/**
	 * Constructs a new PrintStreamMessenger
	 * @param out {@link PrintStream} to print output messages
	 */
	public PrintStreamMessenger(PrintStream out){
		this.out = out;
	}
	
	@Override
	public void breakLine(){
		out.println();
		sameLine = false;
	}

	@Override
	public void sendError(String msg) {
		sameLine = false;
		msg = ERROR_LABEL + msg;
		sendMessage(msg);
	}

	@Override
	public void sendFatalError(String msg) {
		sameLine = false;
		msg = FATAL_ERROR_LABEL + msg;
		sendMessage(msg);
	}
		
	@Override
	public void sendMessage(String msg) {
		if (sameLine){
			out.println(msg);
			sameLine = false;
		} else
			out.println(lineTab + msg);
	}

	@Override
	public void sendWarning(String msg) {
		sameLine = false;
		msg = WARNING_LABEL + msg;
		sendMessage(msg);		
	}

}
