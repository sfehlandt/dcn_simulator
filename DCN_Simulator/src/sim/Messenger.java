package sim;


/**
 * Defines methods for printing output messages, warnings and errors
 *  
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public interface Messenger {
	
	/** Use to break a line in the output */
	public abstract void breakLine();
	
	/** Launches an error message
	 *  @param msg message to launch
	 */
	public abstract void sendError(String msg);
	
	/** Launches a fatal error message and terminates execution
	 *  @param msg message to launch
	 */
	public abstract void sendFatalError(String msg);
	
	/** Sends a general message
	 *  @param msg message to send
	 */
	public abstract void sendMessage(String msg);

	
	/** Launches a warning message
	 *  @param msg message to launch
	 */
	public abstract void sendWarning(String msg);
}
