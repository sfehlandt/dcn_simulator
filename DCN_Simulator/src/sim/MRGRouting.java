package sim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleWeightedGraph;

import network.Flow;
import network.Network;
import network.Node;
import network.Session;
import util.PublicWeightedEdge;

/**
 * Implementation of the Multi-Resource Green Routing (MRG Routing) algorithm to route flow requests in a Network.
 * Implements the RoutingAlgorithm interface
 * @author sfehland
 *
 */
public class MRGRouting implements RoutingAlgorithm{
	/** Identifier of sub-algorithm as Multi-Resource Green Algorithm */
	public static final int MR_GREEN                = 0;
	
	/** Identifier of sub-algorithm as Multi-Resource Green Algorithm 
	 * with pre-reservation of resources in source and target nodes of Flows */
	public static final int MR_GREEN_SAVE_RESOURCES = 1;
	
	/** Identifier of sub-algorithm as Multi-Resource Shortest-Path Algorithm */
	public static final int MR_SPATH                = 2;
	
	/** Identifier of sub-algorithm as Multi-Resource Shortest-Path Algorithm 
	 * with pre-reservation of resources in source and target nodes of Flows */
	public static final int MR_SPATH_SAVE_RESOURCES = 3;
	
	/** Identifier of sub-algorithm as Single-Resource Green Algorithm */
	public static final int SR_GREEN                = 4;
	
	/** Identifier of sub-algorithm as Single-Resource Green Algorithm 
	 * with pre-reservation of resources in source and target nodes of Flows */
	public static final int SR_GREEN_SAVE_RESOURCES = 5;
	
	/** Identifier of sub-algorithm as Single-Resource Shortest-Path Algorithm */
	public static final int SR_SPATH                = 6;
	
	/** Identifier of sub-algorithm as Single-Resource Shortest-Path Algorithm 
	 * with pre-reservation of resources in source and target nodes of Flows */
	public static final int SR_SPATH_SAVE_RESOURCES = 7;
	
	// Communication attributes
	private Messenger msn;
	private int verbosity = VERBOSITY_START_END;
	private int flowCount = 0;
	
	private int subalgorithm = -1;
	private boolean greenAlg       = true;
	private boolean saveResources  = false;
	private boolean singleResource = false;
	
	// Session object
	private Session ses;
	
	// Graph objects (used for routing algorithm):
	protected Set<Node> activeNodes;
	protected SimpleWeightedGraph<Node, PublicWeightedEdge> graph;
	
	/**
	 * Constructs a new MRGRouting algorithm without initialisation.
	 * <p>Requires execution of {@link MRGRouting#init} method before running any simulation
	 */
	public MRGRouting(){}
	
	/**
	 * Constructs and initialise a new MRGRouting algorithm
	 * @param ses {@link Session} object to route
	 * @param msn {@link Messenger} used to print messages, errors and warnings triggered by the algorithm
	 */
	public MRGRouting(Session ses, Messenger msn){
		init(ses, msn);
	}

	//*********************** METHODS DEFINED BY INTERFACE *************************
	@Override
	public void init(Session ses, Messenger msn) {
		this.ses = ses;
		this.msn = msn;
	}
	
	@Override
	public void reset() {
		ses.resetState(false);    // Reset Session State
		
		// Reset graph:
		this.graph       = new SimpleWeightedGraph<Node, PublicWeightedEdge>(PublicWeightedEdge.class);
		//this.allEdges    = new HashSet<PublicWeightedEdge>(ses.net.getNumLinks());
		this.activeNodes = new HashSet<Node>(ses.getNumNodes());
	}
	
	@Override
	public void run(int subalgorithm, int verbosity){
		this.verbosity = verbosity;
		run(subalgorithm);
	}	
	
	@Override
	public void run(int subalg){
		this.subalgorithm = subalg;
		
		//---- Reset before run algorithm:
		reset();
		
		//---- Pre-analyse sub-algorithm:
		saveResources = false;
		decodeSubalgorithm();
		
		//---- Print initial message:
		String subalgName = "";
		
		if (singleResource)
			subalgName = "SR ";		
		else
			subalgName = "MR ";		
		
		if (greenAlg)
			subalgName += "green algorithm";
		
		else
			subalgName += "shortest-path algorithm";
		
		if (saveResources)
			subalgName += " with source/target resource reservation";
		
		if (verbosity > VERBOSITY_ONE_LINE) {
			msn.breakLine();			
			msn.sendMessage("Starting " + subalgName + " to route " + ses.getNumFlows() + " flows: ");
		}
		
		//---- Pre-use resources for source and target nodes of each flow ----//
		if (saveResources){			
			if (verbosity > VERBOSITY_START_END)
				msn.sendMessage("Pre-using resources of source and target nodes of each flow");
			
			saveResourcesOnEnds();
		}		
		
		//---- Execute Routing Algorithm ---/-/
		routingAlg();		
	}
	
	//************************** ROUTING ALGORITHM ****************************	
	/**
	 * Executes the routing algorithm
	 */
	private void routingAlg(){
		
		flowCount = 1;     // processed flows counter
		String msg = "";   // auxiliary String used to build messages before sending them		
		
		//---------------- While we still have pending flows ----------------//	
		while (!ses.isFullyRouted()) {
			Flow candidateFlow = null;
			boolean candidateFound = false;
			Flow flow = null;
			
			if (greenAlg){
				//------------- Search for candidate flow -------------------//
				for (Iterator<Flow> iterator = ses.getFlows().iterator(); iterator.hasNext(); ){
					
					flow = iterator.next();                                       // get flow
					if (flow.getStatus() != Flow.RoutingStatus.PENDING) continue; // if flow is already routed we skip
					
					// if not pre-used, check if source and target nodes have enough resources
					if (!checkFlowEnds(flow, false) ) continue;
					
					// load active nodes capable to carry the flow and their edges into graph:
					loadCapableSubGraph(flow, activeNodes);
					
					// if there is a path between source and target in graph we have found a candidate flow:
					if (isConnected(flow)) {
						candidateFlow = flow;
						candidateFound = true;
						break;
					}
				}
			}
			
			//------- If no candidate flow, we try with a "random" flow -------//
			if (!candidateFound){ 
				candidateFlow = getRandomFlow();			
			}
			
			//-------------------- Process candidate flow -------------------//
			msg = displayProcessingFlow(flowCount, candidateFlow); // display which flow we are processing
			flowCount++;                                           // increase current flow counter
			
			if (!candidateFound) {
				// if not pre-used, check if source and target nodes have enough resources:
				if (!checkFlowEnds(candidateFlow, true) ) continue;
			
				// load all nodes capable to carry the flow and their edges into graph:
				loadCapableSubGraph(candidateFlow, null);
			}
		
			if (greenAlg && !singleResource)     // calculate node and edge weights
				calculateWeights(candidateFlow); 
			
			boolean routeFound = findRoute(candidateFlow);   // find shortest-path route	
			displayProgress(candidateFlow, msg, routeFound); // display progress
		}
		displayOnEnd();
	}
	
	
	//******************* METHODS USED BY ROUTING ALGORITHM *******************
	private void decodeSubalgorithm(){
		if (subalgorithm == MR_GREEN) {
			greenAlg       = true;
			saveResources  = false;
			singleResource = false;
			
		} else if (subalgorithm == MR_GREEN_SAVE_RESOURCES){
			greenAlg       = true;
			saveResources  = true;
			singleResource = false;
			
		} else if (subalgorithm == MR_SPATH) {
			greenAlg       = false;
			saveResources  = false;
			singleResource = false;
			
		} else if (subalgorithm == MR_SPATH_SAVE_RESOURCES){
			greenAlg       = false;
			saveResources  = true;
			singleResource = false;
			
		} else if (subalgorithm == SR_GREEN) {
			greenAlg       = true;
			saveResources  = false;
			singleResource = true;
			
		} else if (subalgorithm == SR_GREEN_SAVE_RESOURCES){
			greenAlg       = true;
			saveResources  = true;
			singleResource = true;
			
		} else if (subalgorithm == SR_SPATH) {
			greenAlg       = false;
			saveResources  = false;
			singleResource = true;
			
		} else if (subalgorithm == SR_SPATH_SAVE_RESOURCES){
			greenAlg       = false;
			saveResources  = true;
			singleResource = true;
		}
	}
	
	/**
	 * For each flow, makes use the resources of their source and target Nodes.
	 * <p>For each case where it is not possible it sends a warning, changes the flow to aborted status 
	 * and the update to the Nodes resources is reverted
	 * @return <code>false</code> if any source or target Node of any flow does not have enough resources,
	 * <code>true</code> otherwise
	 */
	private boolean saveResourcesOnEnds() {
		boolean ret = true;
		List<Flow> flows = ses.getFlows();
		
		for (Flow flow : flows){
			
			int result = flow.saveResourcesOnEnds();
			
			if (result == 1) {
				if (verbosity > VERBOSITY_START_END)
					msn.sendWarning("Source node does not have enough resources to process flow. Skipping flow "+ 
						+ (flowCount++) + " of " + ses.getNumFlows() + ". ");
				
				ses.abortFlow(flow);
				ret = false;
			
			} else if (result == 2) {
				if (verbosity > VERBOSITY_START_END) {
					msn.sendWarning("Target node does not have enough resources to process flow. Skipping flow "+ 
						+ (flowCount++) + " of " + ses.getNumFlows() + ". ");
				}
				
				ses.abortFlow(flow);
				ret = false;
			}
		}
		return ret;
	}
	
	/**
	 * Checks if the sourcen and target nodes of a given flow have enough resources 
	 * (or have saved resources) to process the flow.
	 * @param flow {@link Flow} object to check
	 * @param takeActions boolean indicating if the method should abort the flow 
	 * in case its source and/or target nodes do not have enough resources to process it
	 * @return <code>false</code> if either source or target Node does not have enough resources to process the flow,
	 * <code>true</code> otherwise.
	 */
	private boolean checkFlowEnds(Flow flow, boolean takeActions){
		
		int check = flow.checkEndsNodes();
		
		if (check == 0)	return true;
		
		if (takeActions) {
		
			if (check == 1) {
				if (verbosity > VERBOSITY_START_END)
					msn.sendWarning("Source node does not have enough resources to process flow. Skipping flow");
				
				ses.abortFlow(flow);
			}
		
			if (check == 2) {
				if (verbosity > VERBOSITY_START_END)
					msn.sendWarning("Target node does not have enough resources to process flow. Skipping flow");
				
				ses.abortFlow(flow);
			}
			
			if (verbosity >= VERBOSITY_FLOW_PATH)
				msn.breakLine();
		}
		return false;
	}
	
	/**
	 * Loads into the internal graph the subset of Nodes that are capable to process a given flow, 
	 * @param flow {@link Flow} to consider
	 * @param nodes {@link Set} of {@link Node}s to be checked, 
	 * if the {@link Set} is null then all the nodes are considered
	 */
	private void loadCapableSubGraph(Flow flow, Set<Node> nodes){
		
		// Reset graph:
		graph = new SimpleWeightedGraph<Node, PublicWeightedEdge>(PublicWeightedEdge.class);
		
		// Add capable nodes to graph:
		if (nodes != null) {                        // if there is a set of Nodes we check its elements
			for (Node node : nodes) {
				if ( flow.checkNode(node) ) {
					graph.addVertex(node);
				}
			}
		} else {                                    // if not we check all the nodes in the network
			Node node = null;
			Network net = ses.getNetwork();
			for (int i = 0; i < net.getNumNodes(); i++) {
				if ( flow.checkNode(node = net.getNode(i)) ) {
					graph.addVertex(node);
				}
			}
		}
		
		// Add capable edges to graph:
		Set<Node> graphNodes = graph.vertexSet();
		for (Node node : graphNodes){
			for(int port = 0; port < node.getNumPorts(); port++){
				
				Node adjNode = node.getAdjacentNode(port);
				
				if (graphNodes.contains(adjNode)){
					PublicWeightedEdge edge = graph.addEdge(node, adjNode);
					if (edge != null)  edge.setPorts(port, node.getAdjacentPort(port));
				}			
			}
		}
	}
	
	/**
	 * Checks if the source and target nodes for a given flow are connected in the current graph
	 * @param flow {@link Flow} to check
	 * @return <code>true</code> if flows source and target {@link Node}s are connected, <code>false</code> if not
	 */
	private boolean isConnected(Flow flow){
		Node source = flow.getSource();  // extract flow source node
		Node target = flow.getTarget();  // extract flow target node
		
		if(!graph.containsVertex(source) || !graph.containsVertex(target)) return false;
		
		// check connectivity:
		ConnectivityInspector<Node, PublicWeightedEdge> inspector = 
				new ConnectivityInspector<Node, PublicWeightedEdge>(graph);
		
		return inspector.pathExists(source, target);
	}
	
	/** @return a "random" pending or non-routed {@link Flow} */
	private Flow getRandomFlow(){
		int index = randomWithRange(0, ses.getNumFlows()-1);
		index = 0;
		List<Flow> flows = ses.getFlows();
		
		Flow flow = flows.get(index);
		
		for (Iterator<Flow> iterator = flows.iterator(); 
				iterator.hasNext() && flow.getStatus() != Flow.RoutingStatus.PENDING; flow = iterator.next());
		
		return flow;
	}
	
	/**
	 * Calculates node and edge weights for the given {@link Flow}
	 * @param flow {@link Flow} to consider
	 */
	private void calculateWeights(Flow flow){
		for (Node node : graph.vertexSet()) {
			node.calculateNodeWeight(flow.getDemands());
		}
		
		for (PublicWeightedEdge edge : graph.edgeSet()) {
			double weight= (((Node) edge.getSource()).getNodeWeight() + ((Node) edge.getTarget()).getNodeWeight())/2.0;
			graph.setEdgeWeight(edge, weight);
		}
	}
	
	/**
	 * Finds a shortest-path route for the flow and processes it
	 * @param flow {@link Flow} to be routed
	 * @return <code>true</code> if the flow was routed successfully, <code>false</code> if not
	 */
	private boolean findRoute(Flow flow){
		
		Node source = flow.getSource();  // extract flow source node
		Node target = flow.getTarget();  // extract flow target node
		
		//---- Execute Dijkstra algorithm ----//
		DijkstraShortestPath<Node,PublicWeightedEdge> shortestPath = 
				new DijkstraShortestPath<Node,PublicWeightedEdge>(graph, source, target);
		
		//double pathLength = shortestPath.getPathLength();                // get shortest-path length
		List<PublicWeightedEdge> spEdges = shortestPath.getPathEdgeList(); // get shortest-path as a List of edges		
		
		if (spEdges == null){    // if there is no path or route:
			ses.abortFlow(flow); // abort flow	
			return false;        // and return false                      
		}
		
		//---- Read and process shortest-path route ----//
		ArrayList<Node> spNodes = new ArrayList<Node>(spEdges.size() + 1); // List of Nodes to store the path
		Node edgeSource = null;
		Node edgeTarget = null;
		Node last = source;
		
		if (verbosity >= VERBOSITY_DETAILED_PROGRESS && !last.isOn()) 
			msn.sendMessage("Turning ON Node: " + last);
		
		last.turnOn();         // turn ON node
		spNodes.add(last);     // add source to List
	
		for(PublicWeightedEdge edge : spEdges){
			edgeSource = (Node) edge.getSource();        // get edge source node
			edgeTarget = (Node) edge.getTarget();        // get edge target node
			
			// Identify nodes order relative to the route:
			if (edgeSource.equals(last))
				last = edgeTarget;
			else 
				last = edgeSource;
			
			if (verbosity >= VERBOSITY_DETAILED_PROGRESS && !last.isOn()) 
				msn.sendMessage("Turning ON Node: " + last);			
			
			last.turnOn();                               // turn ON node
			
			edgeSource.turnPortOn(edge.getSourcePort()); // turn ON the corresponding port in the edge source node
			edgeTarget.turnPortOn(edge.getTargetPort()); // turn ON the corresponding port in the edge target node
			
			spNodes.add(last);		  // add Node to list of nodes: spNodes			
		}
		
		ses.routeFlow(flow, spNodes); // route the flow through the route
		activeNodes.addAll(spNodes);  // add the nodes of the route to the Set of active nodes

		return true;
	}
	
	//************************* DISPLAY METHODS *******************************
	/**
	 * Displays message indicating which flow is being processed
	 * @param flowCount counter of the current flow being processed
	 * @param candidateFlow {@link Flow} being processed
	 * @return a String message that might be used further down the road by the algorithm
	 */
	private String displayProcessingFlow(int flowCount, Flow candidateFlow){
		String msg = "";
		if (verbosity >= VERBOSITY_FLOW_PROGRESS) {
			msg = "Processing flow "+flowCount+" of "+ses.getNumFlows() + ". ";
		
			if (verbosity >= VERBOSITY_FLOW_PATH) {
				msg +=": " + candidateFlow;
				msn.sendMessage(msg);
				msg = "";
			}				
		}
		return msg;
	}
	
	/**
	 * Displays messages after routing a flow, such as the route and 
	 * power consumption (depending on verbosity level)
	 * @param candidateFlow {@link Flow} flow that was just routed
	 * @param msg String to be included prior to the message
	 * @param routeFound boolean indicating if the algorithm found a route or not, 
	 * in order to send the appropriate message
	 */
	private void displayProgress(Flow candidateFlow, String msg, boolean routeFound){
		if (verbosity < VERBOSITY_FLOW_PROGRESS) return;
		
		if (routeFound) {
			if (verbosity >= VERBOSITY_FLOW_PATH) {
				msg = "Routed successfully, route = " + candidateFlow.toStringRoute(false);
				msn.sendMessage(msg);
				msg = "";
				
			} else {
				msg += "Routed successfully. ";
			}
			
			msg += "Network power consumption so far = " + ses.getNetwork().powerConsumption() + "[W]";
			
		} else {
			msg += "The flow could not be routed, skipping flow.";
		}
		
		msn.sendMessage(msg);
		if (verbosity >= VERBOSITY_FLOW_PATH) msn.breakLine();
	}
	
	private void displayOnEnd() {
		if (verbosity <= VERBOSITY_ZERO) return;
		
		if (verbosity >= VERBOSITY_START_END) {
			msn.sendMessage("Routing finished, " + ses.getNumRoutedFlows() + " of " + ses.getNumFlows() + 
				" flows routed successfully");
		}
		
		String msg = "";
		String subalgName = "";
		
		if (verbosity == VERBOSITY_ONE_LINE) {
			
			if (singleResource)
				subalgName = "SR ";		
			else
				subalgName = "MR ";		
			
			if (greenAlg)
				subalgName += "Green";			
			else
				subalgName += "SPath";
			
			if (saveResources)
				subalgName += "(res), ";
			else
				subalgName += ",      ";
			
			msg += subalgName;

		} else {
			msg += "Results: ";
		}
		
		String sep = ", \t";
		msg +=     "Nr of flows = "            +  ses.getNumFlows()                            + sep + 
				   "Nr of non-routed flows = " + (ses.getNumFlows() - ses.getNumRoutedFlows()) + sep +
				   "Nr of active nodes = "     +  ses.getNumNodesON()                          + sep +
				   "Nr of congested nodes = "  +  ses.getNumNodesCongested()                   + sep +
				   "Power consumption = "      +  ses.getPowerConsumption()  + "[W]";
		
		msn.sendMessage(msg);
	}
	
	
	//************************* OTHER METHODS *******************************
	/** Returns a random integer within the range [min, max]
	 *  @param min minimum possible value
	 *  @param max maximum possible value
	 *  @return random integer within the range [min, max]
	 */
	private int randomWithRange(int min, int max){
	   int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
}
