package sim;

import network.Session;

/**
 * The root interface in the routing algorithms hierarchy.
 * 
 * <p>Defines the methods to be implemented by classes implementing routing algorithms.</p>
 * @author Sebastian Fehlandt
 * @since Aug 2016
 */
public interface RoutingAlgorithm {
	
	/** Verbosity level indicating that no messages will be printed by the routing algorithm */
	public static final int VERBOSITY_ZERO              = 0;
	
	/** Verbosity level indicating that the routing algorithm will only print 1 summary line */
	public static final int VERBOSITY_ONE_LINE          = 1;
	
	/** Verbosity level indicating that the routing algorithm will print only 
	 * at the beggining and end of its execution */
	public static final int VERBOSITY_START_END         = 2;
	
	/** Verbosity level indicating that the routing algorithm will print its progress */
	public static final int VERBOSITY_FLOW_PROGRESS     = 3;
	
	/** Verbosity level indicating that the routing algorithm will print its progress 
	 * including the route found for each flow */
	public static final int VERBOSITY_FLOW_PATH         = 4;
	
	/** Verbosity level indicating that the routing algorithm will print its progress 
	 * including the route found for each flow, and which nodes are turned ON in each iteration */
	public static final int VERBOSITY_DETAILED_PROGRESS = 5;
	
	/**
	 * Initialises the routing algorithm to be applied to a given {@link Session} object
	 * @param ses Session object to apply the algorithm to
	 * @param msn {@link Messenger} to send messages, warnings and errors.
	 */
	public abstract void init(Session ses, Messenger msn);
	
	/** Resets the algorithm and Session*/
	public abstract void reset();
	
	/**
	 * Runs the sub algorithm given as parameter
	 * @param subalgorithm indicates a sub-algorithm to use, 0 should be always the default sub-algorithm
	 */
	public abstract void run(int subalgorithm);
	
	/**
	 * Runs the sub algorithm given as parameter with a certain verbosity level for printing messages
	 * @param subalgorithm indicates a sub-algorithm to use, 0 should be always the default sub-algorithm
	 * @param verbosity indicates the level of detail up to which progress messages should be shown.
	 * The verbosity level is stored in the object and will be used until it is changed by this or another method
	 */
	public abstract void run(int subalgorithm, int verbosity);
}
